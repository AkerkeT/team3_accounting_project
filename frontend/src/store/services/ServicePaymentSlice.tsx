import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';

export interface IServiceForm {
  title: string;
  quantity: number;
  price: number;
  sum: number;
}

export interface IServicePayment {
  _id: string;
  startDate: Date;
  clientId: string | null;
  totalSum: number;
  serviceElements: IServiceForm[];
}

export interface INewServicePayment {
  startDate: Date;
  clientId: string | undefined;
  totalSum: number;
  serviceElements: IServiceForm[];
}

export interface EditingServicePayment {
  id: string | undefined;
  data: {
    serviceElements: IServiceForm[];
    startDate: Date;
    clientId: string | undefined;
    totalSum: number;
  };
}

type id = string | undefined;

export const getServicePayment = createAsyncThunk('get/servicePayment', async () => {
  const res = await axios.get('/admin/client/servicePayment');
  return res.data;
});

export const postServicePayment = createAsyncThunk(
  'add/servicePayment',
  async (newData: INewServicePayment, thunkApi) => {
    try {
      const res = await axios.post('/admin/client/servicePayment', newData);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(servicePaymentSlice.actions.catchError(err.response.data));
      }
    }
  }
);

export const getServicePaymentByClient = createAsyncThunk(
  'get/current/client/servicePayment',
  async (id: id) => {
    const res = await axios.get(`/admin/client/servicePayment?client=${id}`);
    return res.data;
  }
);

export const editServicePayment = createAsyncThunk(
  'put/servicePayment',
  async (updatedData: EditingServicePayment, thunkApi) => {
    try {
      const res = await axios.put(
        `/admin/client/servicePayment/${updatedData.id}`,
        updatedData.data
      );
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(servicePaymentSlice.actions.catchError(err.response.data));
      }
    }
  }
);

interface IServicePaymentState {
  servicePaymentList: IServicePayment[];
  currentServicePaymentList: IServicePayment[];
  hasError: boolean;
  addedSuccessfully: boolean;
  servicePaymentId: string | null;
  isLoading: boolean;
  message: string | null;
}

const initialState: IServicePaymentState = {
  servicePaymentList: [],
  currentServicePaymentList: [],
  servicePaymentId: null,
  hasError: false,
  addedSuccessfully: false,
  isLoading: false,
  message: null,
};

const servicePaymentSlice = createSlice({
  name: 'servicePayment',
  initialState,
  reducers: {
    resetServicePayment: (state: IServicePaymentState) => {
      state.hasError = false;
      state.isLoading = false;
      state.message = null;
      state.addedSuccessfully = false;
      state.servicePaymentList = [];
    },
    resetServicePaymentId: (state: IServicePaymentState) => {
      state.servicePaymentId = null;
    },
    catchError: (state, action) => {
      state.hasError = true;
      state.message = action.payload?.message;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getServicePayment.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getServicePayment.fulfilled, (state, action) => {
        state.isLoading = false;
        state.servicePaymentList = action.payload;
      })
      .addCase(getServicePaymentByClient.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getServicePaymentByClient.fulfilled, (state, action) => {
        state.isLoading = false;
        state.currentServicePaymentList = action.payload;
        if (action.payload[0]) {
          state.servicePaymentId = action.payload[0]._id;
        } else {
          state.servicePaymentId = null;
        }
      })
      .addCase(postServicePayment.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(postServicePayment.fulfilled, (state, action) => {
        state.currentServicePaymentList.push(action.payload.service);
        state.servicePaymentId = action.payload.service._id;
        state.addedSuccessfully = true;
        state.message = action.payload.message;
        state.isLoading = false;
      })
      .addCase(editServicePayment.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(editServicePayment.fulfilled, (state, action) => {
        state.isLoading = false;
        state.addedSuccessfully = true;
        state.message = action.payload.message;
      });
  },
});

export const { resetServicePayment, resetServicePaymentId, catchError } =
  servicePaymentSlice.actions;
export default servicePaymentSlice.reducer;
