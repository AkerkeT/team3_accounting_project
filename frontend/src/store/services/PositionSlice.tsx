import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';

export interface IPosition {
  _id: string;
  title: string | undefined;
  headPositionId: string | undefined;
}

interface IPositionState {
  error: string | null;
  loading: boolean;
  positions: IPosition[];
  position: IPosition | null;
  openEditModal: boolean;
  successfully: boolean;
  message: string | null;
  hasError: boolean;
}

type id = string | undefined;

interface newDirectory {
  title: string;
  headPositionId: string | null;
}

export const getPositions = createAsyncThunk('get/positions', async () => {
  const res = await axios.get('/admin/positions/');
  return res.data;
});

export const getPositionById = createAsyncThunk('get/positions/id', async (id: id) => {
  const res = await axios.get(`/admin/positions/${id}`);
  return res.data;
});

export const createPosition = createAsyncThunk(
  'create/positions/',
  async (newDirectory: newDirectory, thunkApi) => {
    try {
      const res = await axios.post(`/admin/positions`, newDirectory);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(positionsSlice.actions.catchError(err.response.data));
      }
    }
  }
);

export const editPosition = createAsyncThunk(
  'edit/positions/',
  async (directory: IPosition, thunkApi) => {
    try {
      const res = await axios.put(`/admin/positions/${directory._id}`, directory);
      return res.data;
    } catch (e) {
      const err = e as AxiosError;
      if (err.response && err.response.data) {
        thunkApi.dispatch(positionsSlice.actions.catchError(err.response.data));
      }
    }
  }
);

const initialState: IPositionState = {
  error: null,
  loading: false,
  positions: [],
  position: null,
  openEditModal: false,
  successfully: false,
  message: null,
  hasError: false,
};

const positionsSlice = createSlice({
  name: 'positions',
  initialState,
  reducers: {
    setPositions: (state, action: PayloadAction<IPosition[]>) => {
      state.positions = action.payload;
    },
    resetPosition: (state) => {
      state.position = null;
    },
    resetStatus: (state) => {
      state.message = null;
      state.successfully = false;
    },
    editedPosition: (state, action) => {
      const copy = state.positions;
      const index = state.positions.findIndex((element) => element._id === action.payload._id);
      copy[index] = action.payload;
      state.positions = copy;
    },
    catchError: (state, action) => {
      state.hasError = true;
      state.message = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getPositions.pending, (state) => {
        state.loading = true;
      })
      .addCase(getPositions.fulfilled, (state, action) => {
        state.loading = false;
        state.positions = action.payload;
      })
      .addCase(getPositions.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'Something went wrong';
      })
      .addCase(getPositionById.pending, (state) => {
        state.loading = true;
      })
      .addCase(getPositionById.rejected, (state, action) => {
        state.error = action.error.message || 'Something went wrong';
        state.loading = false;
      })
      .addCase(getPositionById.fulfilled, (state, action) => {
        state.loading = false;
        state.position = action.payload;
      })
      .addCase(createPosition.pending, (state) => {
        state.loading = true;
      })
      .addCase(createPosition.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || 'error create position';
      })
      .addCase(createPosition.fulfilled, (state, action) => {
        state.loading = false;
        state.positions.push(action.payload.newPosition);
        state.successfully = true;
        state.message = action.payload.text;
      })
      .addCase(editPosition.pending, (state) => {
        state.loading = true;
      })
      .addCase(editPosition.rejected, (state, action) => {
        state.error = action.error.message || 'error edit position';
        state.loading = false;
      })
      .addCase(editPosition.fulfilled, (state, action) => {
        state.loading = false;
        state.message = action.payload.text;
        state.successfully = true;
      });
  },
});

export const { setPositions, resetPosition, editedPosition, resetStatus } = positionsSlice.actions;
export default positionsSlice.reducer;
