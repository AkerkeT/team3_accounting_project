import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from '../../axiosApi';
import { Elements } from './KpiSlice';

export interface IKpiHistory {
  _id: string;
  KpiID: string;
  kpiElements: Elements[];
  positionId: {
    _id: string;
    title: string;
  };
  percentForKpi: number;
  dateOfEvent: Date;
}

interface IKpiHistoryState {
  loading: boolean;
  kpiHistory: Array<IKpiHistory>;
}

type id = string | null;

export const getKpiHistoryById = createAsyncThunk('get/kpihistory/id', async (id: id) => {
  const res = await axios.get(`/admin/kpiHistory/${id}`);
  return res.data;
});

const initialState: IKpiHistoryState = {
  loading: false,
  kpiHistory: [],
};

const KpiHistoriesSlice = createSlice({
  name: 'kpiHistory',
  initialState,
  reducers: {
    resetState: (state) => {
      state.kpiHistory = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getKpiHistoryById.pending, (state) => {
        state.loading = true;
      })
      .addCase(getKpiHistoryById.fulfilled, (state, action) => {
        state.loading = false;
        state.kpiHistory = action.payload;
      });
  },
});
export const { resetState } = KpiHistoriesSlice.actions;
export default KpiHistoriesSlice.reducer;
