import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from '../../axiosApi';
import { IServiceForm } from './ServicePaymentSlice';

export interface IServicePaymentHistory {
  _id: string;
  startDate: Date;
  clientId: string | null;
  totalSum: number;
  servicePaymentId: string;
  dateOfEvent: Date;
  serviceElements: IServiceForm[];
}

interface IKpiHistoryState {
  loadingIs: boolean;
  servicePaymentHistory: Array<IServicePaymentHistory>;
}

type id = string | null;

export const getServicePaymentHistoryById = createAsyncThunk(
  'get/servicePaymentHistory/id',
  async (id: id) => {
    const res = await axios.get(`/admin/client/servicePaymentHistory/${id}`);
    return res.data;
  }
);

const initialState: IKpiHistoryState = {
  loadingIs: false,
  servicePaymentHistory: [],
};

const ServicePaymentHistorySlice = createSlice({
  name: 'servicePaymentHistory',
  initialState,
  reducers: {
    resetState: (state) => {
      state.servicePaymentHistory = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getServicePaymentHistoryById.pending, (state) => {
        state.loadingIs = true;
      })
      .addCase(getServicePaymentHistoryById.fulfilled, (state, action) => {
        state.loadingIs = false;
        state.servicePaymentHistory = action.payload;
      });
  },
});
export const { resetState } = ServicePaymentHistorySlice.actions;
export default ServicePaymentHistorySlice.reducer;
