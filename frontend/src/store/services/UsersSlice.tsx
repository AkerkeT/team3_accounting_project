import { NavigateFunction } from 'react-router-dom';
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AxiosError } from 'axios';
import axios from '../../axiosApi';
import { IEmployee } from './EmployeesListSlice';
import { IPosition } from './PositionSlice';
interface errorType {
  message: string;
}
interface IUser {
  _id: string;
  login: string;
  password: string;
  roleId: string;
  employee?: IEmployee;
  positionId?: IPosition;
}

export interface tokenData {
  token: string | null;
  iat?: number;
  exp?: number;
}
export interface userInfo {
  tokenData: tokenData;
  user?: IUser;
}
export interface IUsersSliceState {
  loginError?: errorType | null;
  user: userInfo | null | undefined;
  isLoading?: boolean | null;
  global?: string | null;
}

interface IUserForm {
  userData: {
    login: string;
    password: string;
  };
  navigate: NavigateFunction;
}

const initialState: IUsersSliceState = {
  loginError: null,
  user: null,
  isLoading: false,
};

export const loginUser = createAsyncThunk(
  'users/login',
  async (payload: IUserForm, thunkApi): Promise<userInfo | undefined> => {
    try {
      const res = await axios.post('/users/login', payload.userData);
      payload.navigate('/');
      localStorage.setItem('token', JSON.stringify(res.data.userInfo));
      return res.data.userInfo;
    } catch (e) {
      const err = e as AxiosError;
      if (err?.response?.data) {
        thunkApi.dispatch(usersSlice.actions.catchLoginError(err.response.data));
      } else {
        thunkApi.dispatch(usersSlice.actions.globalError(err.message));
      }
    }
  }
);

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    logoutUser: (state) => {
      state.user = null;
      localStorage.removeItem('token');
    },
    catchLoginError: (state, action) => {
      state.loginError = action.payload;
    },
    globalError: (state, action) => {
      state.global = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginUser.pending, (state) => {
        state.loginError = null;
      })
      .addCase(loginUser.fulfilled, (state, action: PayloadAction<userInfo | undefined>) => {
        state.user = action?.payload;
        state.loginError = null;
      });
  },
});

export const { logoutUser, catchLoginError } = usersSlice.actions;
export default usersSlice.reducer;
