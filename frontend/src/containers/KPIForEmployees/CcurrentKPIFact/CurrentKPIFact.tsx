import { useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { Link, useParams } from 'react-router-dom';
import { Breadcrumb, Descriptions, Table, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { getKPIFact, IKPIFactElements } from '../../../store/services/KPIFactSlice';
import { getEmployee } from '../../../store/services/KpiSlice';
export const CurrenKPIFact = () => {
  const { Title } = Typography;
  const { currentKpiFact, currentKpiFactElements } = useAppSelector(
    (state) => state.kpiFact,
    shallowEqual
  );

  const [averall, setAverAll] = useState(0);

  const dispatch = useAppDispatch();
  const params = useParams();
  useEffect(() => {
    dispatch(getKPIFact(params.id));
    dispatch(getEmployee());
  }, [dispatch]);

  useEffect(() => {
    if (currentKpiFactElements) {
      let percent = 0;
      let number = 0;
      currentKpiFactElements.map((fact) => {
        if (fact.factWeight !== null) {
          percent += fact.factWeight;
          number += 1;
        }
      });
      const num = percent / number;
      setAverAll(num);
    }
  }, [currentKpiFactElements]);

  const columns: ColumnsType<IKPIFactElements> = [
    {
      title: 'Вид',
      dataIndex: 'view',
      key: 'view',
    },
    {
      title: 'Цель',
      dataIndex: 'goal',
      key: 'goal',
    },
    {
      title: 'Примечание',
      dataIndex: 'comment',
      key: 'comment',
    },
    {
      title: 'Вес',
      dataIndex: 'weight',
      key: 'weight',
    },
    {
      title: 'Факт',
      dataIndex: 'factWeight',
      key: 'factWeight',
    },
    {
      title: 'Комментарий',
      dataIndex: 'employeeComment',
      key: 'employeeComment',
    },
  ];

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link to="/">Главная</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link to="/kpi_employee">КРІ</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Факт KPI за месяц</Breadcrumb.Item>
        </Breadcrumb>
        <Typography style={{ marginTop: '5px' }}>
          Дата и время создания: {currentKpiFact?.createdDate}
        </Typography>
      </div>
      <Title style={{ fontSize: '30px' }}>Факт KPI и выплаты за месяц</Title>
      {currentKpiFact ? (
        <Table
          dataSource={currentKpiFactElements}
          rowKey={() => Math.random() + Math.random()}
          columns={columns}
        />
      ) : null}

      <Descriptions
        title={`Подробнее по KPI и заработной плате за ${currentKpiFact?.month} месяц ${currentKpiFact?.year} года`}
        bordered
        column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      >
        <Descriptions.Item style={{ width: '300px' }} label="Средний процент по KPI">
          {isNaN(averall) ? null : averall}
        </Descriptions.Item>
        <Descriptions.Item style={{ width: '300px' }} label="Максимум KPI">
          0
        </Descriptions.Item>
      </Descriptions>

      <Descriptions bordered column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
        <Descriptions.Item style={{ width: '300px' }} label="Бонус за выполнение KPI">
          0
        </Descriptions.Item>
        <Descriptions.Item style={{ width: '300px' }} label="Фиксированная зарплата">
          {currentKpiFact?.totalSalary.toLocaleString('ru')}
        </Descriptions.Item>
      </Descriptions>

      <Descriptions bordered column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}>
        <Descriptions.Item style={{ width: '300px' }} label="Выплаты за разовые услуги">
          0
        </Descriptions.Item>
        <Descriptions.Item style={{ width: '300px' }} label="Итого к начислению">
          0
        </Descriptions.Item>
      </Descriptions>
    </>
  );
};
