import { useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import {
  Button,
  Card,
  Descriptions,
  Form,
  InputNumber,
  Modal,
  Select,
  Space,
  Typography,
} from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { checkMonth } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import { editFactForMonth, resetCurrentClient } from '../../../store/services/FactForMonthSlice';
import { getServiceElementsList } from '../../../store/services/ServiceElementsSlice';
interface ModifiedIServiceForm {
  serviceElements: [
    {
      id: string;
      title: string;
      quantity: number;
      price: number;
      priceForEmployee: number;
      factQuantity: number;
      sum: number;
    }
  ];
}
interface Props {
  showModal: boolean;
  closeModal: () => void;
}

export const AddFactOfServicePaymentDrawer = ({ showModal, closeModal }: Props) => {
  const [fields, setFields] = useState([]);
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const closeModalHandler = () => {
    setFields([]);
    form.resetFields();
    dispatch(resetCurrentClient());
    closeModal();
  };
  useEffect(() => {
    dispatch(getServiceElementsList());
  }, []);
  const { currentClient } = useAppSelector((state) => state.factForMonth, shallowEqual);
  const [serviceElementsOptions, setServiceElementsOptions] = useState([
    {
      label: '',
      value: '',
    },
  ]);
  const { currentMonth } = useAppSelector((state) => state.factForMonth);
  const { serviceElementsList } = useAppSelector((state) => state.serviceElements);
  useEffect(() => {
    const AllElements = serviceElementsList.map((option): any => {
      const elementPrice = {
        label: option.title,
        value: option.title,
      };
      return elementPrice;
    });
    setServiceElementsOptions(AllElements);
  }, []);

  useEffect(() => {
    setFields([]);
    form.resetFields();
  }, [currentClient]);

  const onFinish = (values: ModifiedIServiceForm) => {
    const { serviceElements } = values;
    const countTotal = values.serviceElements
      .map((item) => {
        const fact = item.factQuantity * item.priceForEmployee;
        return fact;
      })
      .reduce((acc, number) => acc + number);
    let difference = 0;
    if (currentClient?.totalSum) {
      difference = currentClient?.totalSum - countTotal;
    }
    const obj = {
      _id: currentClient?._id,
      date: new Date().toDateString(),
      month: currentMonth?.month,
      clientId: currentClient?.clientId,
      startDate: currentClient?.startDate,
      status: 'Создан',
      difference: difference,
      factTotalSum: countTotal,
      totalSum: currentClient?.totalSum,
      serviceElements: serviceElements,
    };
    if (obj._id !== undefined) {
      dispatch(editFactForMonth({ id: currentMonth?._id, data: obj }));
    }

    setFields([]);
    form.resetFields();
    dispatch(resetCurrentClient());
    closeModal();
  };

  return (
    <>
      {currentMonth?.status === 'Обработан' ? (
        <>
          <Modal
            open={showModal}
            onCancel={closeModalHandler}
            width={'1200px'}
            cancelText="Назад"
            okText=""
          >
            <Descriptions title="Информация о факте оказанных услуг">
              <Descriptions.Item label="Клиент">{currentClient?.clientId?.name}</Descriptions.Item>
              <Descriptions.Item label="Сумма абонплаты">
                {currentClient?.totalSum}
              </Descriptions.Item>
              <Descriptions.Item label="Общая фактическая сумма">
                {currentClient?.factTotalSum}
              </Descriptions.Item>
              <Descriptions.Item label="Разница">{currentClient?.difference}</Descriptions.Item>
            </Descriptions>
            <Descriptions title="Элементы по оказанным услугам">
              {currentClient?.serviceElements.map((element) => {
                return (
                  <div key={element._id}>
                    <Card title={element.title} bordered={false} style={{ width: 300 }}>
                      <Typography>Кол-во по контракту: {element.quantity}</Typography>
                      <Typography>Факт. кол-во: {element.factQuantity}</Typography>
                      <Typography>Цена для сотрудников: {element.priceForEmployee}</Typography>
                    </Card>
                  </div>
                );
              })}
            </Descriptions>
          </Modal>
        </>
      ) : (
        <>
          <Modal
            open={showModal}
            okText="Сохранить"
            onOk={() => form.submit()}
            onCancel={closeModalHandler}
            width={'1200px'}
            cancelText="Отмена"
          >
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <h3>Добавление факта оказанных услуг</h3>
              <>{checkMonth(currentMonth?.month)}</>
            </div>
            <h3>{currentClient?.clientId?.name}</h3>
            <Form
              fields={fields}
              name="dynamic_form_nest_item"
              autoComplete="off"
              form={form}
              onFinish={onFinish}
              style={{ marginTop: '20px' }}
            >
              <Form.List name="serviceElements" initialValue={currentClient?.serviceElements}>
                {(fields, { add, remove }) => (
                  <>
                    {fields.map(({ key, name, ...restField }) => {
                      return (
                        <Space
                          key={key}
                          style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            marginBottom: 8,
                          }}
                          align="baseline"
                        >
                          <Form.Item
                            {...restField}
                            name={[name, 'title']}
                            rules={[
                              {
                                required: true,
                                pattern: new RegExp(/^\S/),
                                message: 'Выберите Наименование услуги',
                              },
                            ]}
                          >
                            <Select style={{ width: 120 }} options={serviceElementsOptions} />
                          </Form.Item>
                          <Form.Item {...restField} name={[name, 'quantity']}>
                            <InputNumber disabled placeholder="кол-во" />
                          </Form.Item>
                          <Form.Item
                            {...restField}
                            name={[name, 'factQuantity']}
                            rules={[
                              {
                                required: true,
                                message: 'Пожалуйста, Введите фактическое количество',
                              },
                              {
                                pattern: new RegExp(/^[0-9]{1,}$/),
                                message: 'Фактическое кол-во должно состоять только из цифр',
                              },
                              {
                                pattern: new RegExp(/^\S/),
                                message: 'Поле не должно содержать лишних пробелов',
                              },
                            ]}
                          >
                            <InputNumber
                              placeholder="Факт. кол-во"
                              style={{ width: '150px' }}
                              min={0}
                            />
                          </Form.Item>
                          <Form.Item {...restField} name={[name, 'priceForEmployee']}>
                            <InputNumber placeholder="Цена" style={{ width: '150px' }} min="0" />
                          </Form.Item>
                          <MinusCircleOutlined onClick={() => remove(name)} />
                        </Space>
                      );
                    })}
                    <Form.Item>
                      <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                        Добавить Факт
                      </Button>
                    </Form.Item>
                  </>
                )}
              </Form.List>
            </Form>
          </Modal>
        </>
      )}
    </>
  );
};
