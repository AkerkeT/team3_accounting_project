import { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Breadcrumb, Button, Modal, Select, Table, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { numberWithCommas, openNotification } from '../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../hooks';
import {
  deleteCurrentMonth,
  FactForMonth,
  getAllYears,
  getFactForMonth,
  resetCurrentYearFacts,
  resetNotification,
  setSelectedYear,
} from '../../store/services/FactForMonthSlice';
import { AddNewFact } from './AddNewFact/AddNewFact';

const { Title } = Typography;

export const FactOfService = () => {
  const navigate = useNavigate();
  const showDrawer = (record: FactForMonth) => {
    navigate(`/fact_of_service/${record._id}`);
  };
  const {
    factForMonths,
    message,
    hasErrors,
    added,
    successfully,
    deleted,
    years,
    selectedYear,
    monthClosed,
    loading,
  } = useAppSelector((state) => state.factForMonth);
  const [yearTotalFactFee, setYearTotalFactFee] = useState(0);
  const [yearTotalServiceFee, setYearTotalServiceFee] = useState(0);
  const [yearTotalPayouts, setTotalPayouts] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [optionForSelect, setOptionsForSelect] = useState([
    {
      value: 0,
      label: 0,
    },
  ]);
  const dispatch = useAppDispatch();
  useEffect(() => {
    const options = years.map((year) => {
      return { label: year, value: year };
    });
    setOptionsForSelect(options);
  }, [years]);
  useEffect(() => {
    dispatch(getAllYears());
  }, [dispatch]);

  useEffect(() => {
    if (factForMonths.length !== 0) {
      const totalYearServiceFee = factForMonths
        .map((item) => item.monthTotalServicePaymentsFee)
        .reduce((acc: number, number: number) => acc + number);
      setYearTotalServiceFee(totalYearServiceFee);
      const totalYearFactFee = factForMonths
        .map((item) => item.monthfactTotalSum)
        .reduce((acc: number, number: number) => acc + number);
      setYearTotalFactFee(totalYearFactFee);
      setTotalPayouts(0);
    }
  }, [factForMonths]);
  useEffect(() => {
    if (hasErrors) {
      openNotification('error', 'bottomLeft', message);
      setInterval(() => {
        dispatch(resetNotification());
      }, 100);
    }
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      if (deleted) {
        dispatch(resetCurrentYearFacts());
        dispatch(getAllYears());
        setYearTotalFactFee(0);
        setYearTotalServiceFee(0);
        if (selectedYear !== null) dispatch(getFactForMonth(selectedYear));
      }
      if (monthClosed) {
        if (selectedYear !== null) dispatch(getFactForMonth(selectedYear));
      }
      if (added) {
        if (selectedYear !== null) {
          dispatch(resetCurrentYearFacts());
          dispatch(getAllYears());
          dispatch(getFactForMonth(selectedYear));
        }
      }
      setInterval(() => {
        dispatch(resetNotification());
      }, 100);
    }
  }, [hasErrors, successfully, dispatch]);
  const confirmToDelete = (value: FactForMonth) => {
    Modal.confirm({
      title: 'Удаление Факта за месяц',
      icon: <ExclamationCircleOutlined />,
      content: 'Хотите удалить безвозвратно Факт за месяц?',
      onOk: () => deleteHandler(value),
      okText: 'Да',
      cancelText: 'Нет',
    });
  };

  const deleteHandler = (record: FactForMonth) => {
    dispatch(deleteCurrentMonth(record._id));
  };
  const closeModal = () => {
    setShowModal(!showModal);
  };
  const selectedYearHandler = (value: number) => {
    dispatch(getFactForMonth(value));
    dispatch(setSelectedYear(value));
    setYearTotalFactFee(0);
    setYearTotalServiceFee(0);
  };
  const columns: ColumnsType<FactForMonth> = [
    {
      title: 'Месяц',
      dataIndex: 'month',
      key: 'month',
    },
    {
      title: 'Абонплата',
      dataIndex: 'monthTotalServicePaymentsFee',
      key: 'monthTotalServicePaymentsFee',
      render: (_, record) => record.monthTotalServicePaymentsFee.toLocaleString('ru'),
    },
    {
      title: 'Факт',
      dataIndex: 'monthfactTotalSum',
      key: 'monthfactTotalSum',
      render: (_, record) => record.monthfactTotalSum.toLocaleString('ru'),
    },
    {
      title: 'Разница',
      dataIndex: 'difference',
      key: 'difference',
      render: (_, record) => record.difference.toLocaleString('ru'),
    },
    {
      title: 'Выплачено',
      dataIndex: 'paid',
      key: 'paid',
    },
    {
      title: 'Статус',
      dataIndex: 'status',
      key: 'status',
    },
    {
      key: 'more',
      title: 'Действие',
      render: (record: FactForMonth) => {
        return (
          <>
            <Button onClick={() => showDrawer(record)} type="primary">
              Подробнее
            </Button>
            <Button
              style={{ marginLeft: '5px' }}
              onClick={() => confirmToDelete(record)}
              type="primary"
              danger
            >
              Удалить
            </Button>
          </>
        );
      },
    },
  ];

  return (
    <div style={{ display: 'flex', justifyContent: 'center', flexDirection: 'column' }}>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Оказанные услуги</Breadcrumb.Item>
      </Breadcrumb>
      <Title>Факт оказания услуг</Title>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          position: 'relative',
        }}
      >
        <Select
          style={{ width: '90px' }}
          value={selectedYear}
          options={optionForSelect}
          size="large"
          onChange={selectedYearHandler}
          placeholder="Выберите год"
        />
        <Button
          style={{ margin: '15px', position: 'absolute', top: '17px', left: '100px' }}
          type="primary"
          onClick={closeModal}
        >
          Создать
        </Button>
        <div style={{ marginRight: '30px', padding: '15px' }}>
          <Typography>
            Итог абонентской платы за {selectedYear} г.: {numberWithCommas(yearTotalServiceFee)} тг.
          </Typography>
          <Typography>
            Итог фактического сопровождения клиентов: {numberWithCommas(yearTotalFactFee)} тг.
          </Typography>
          <Typography>Выплачено за сопровождение клиентов: {yearTotalPayouts}</Typography>
        </div>
      </div>
      {factForMonths?.length === 0 ? (
        <div style={{ alignSelf: 'center', marginTop: '50px' }}>
          <h2>Выберите год для получения данных</h2>
        </div>
      ) : (
        <>
          <Table
            loading={loading}
            columns={columns}
            dataSource={factForMonths}
            size="small"
            rowKey={(record) => record?._id}
          />
        </>
      )}

      <AddNewFact showModal={showModal} closeModal={closeModal} />
    </div>
  );
};
