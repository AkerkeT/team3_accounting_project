import { ChangeEvent, useEffect } from 'react';
import { Button, Drawer, Form, FormInstance, Input, Space } from 'antd';
import { useAppDispatch } from '../../../../hooks';
import { getServiceElementsList } from '../../../../store/services/ServiceElementsSlice';

interface Props {
  closeEditDrawerHandler?: () => void;
  inputEditChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  editRates?: {
    elementsOfService: string;
    price: number;
  };
  onFinishFailed?: () => void;
  sendEditRatesHandler?: () => void;
  form?: FormInstance;
  isEditDrawer?: boolean;
}

export const EditRates = ({
  closeEditDrawerHandler,
  isEditDrawer,
  form,
  sendEditRatesHandler,
  onFinishFailed,
  editRates,
  inputEditChangeHandler,
}: Props) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    form ? form.setFieldsValue(editRates) : null;
  }, [form, editRates]);

  useEffect(() => {
    dispatch(getServiceElementsList());
  }, [dispatch]);

  return (
    <>
      <Drawer
        title="Редактировать тариф"
        placement="right"
        onClose={closeEditDrawerHandler}
        open={isEditDrawer}
      >
        <Space direction="vertical">
          <Form
            form={form}
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: false }}
            onFinish={sendEditRatesHandler}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <label>
              <span style={{ color: 'red' }}>*</span> Элементы услуги:
            </label>
            <Form.Item
              name="elementsOfService"
              rules={[{ required: true, message: 'Пожалуйста, выберите услугу' }]}
              initialValue={editRates?.elementsOfService}
            >
              <Input disabled />
            </Form.Item>
            <label>
              <span style={{ color: 'red' }}>*</span> Цена:
            </label>
            <Form.Item
              name="price"
              rules={[
                { required: true, message: 'Пожалуйста, введите цену для тарифа' },
                {
                  pattern: new RegExp(/^[0-9]{1,}$/),
                  message: 'Цена должен состоять только из цифр',
                },
                {
                  pattern: new RegExp(/^\S/),
                  message: 'поле не должно содержать лишних пробелов',
                },
              ]}
              initialValue={editRates?.price}
            >
              <Input name="price" placeholder="Цена" onChange={inputEditChangeHandler} />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 0 }}>
              <Button type="primary" htmlType="submit" style={{ margin: '15px' }} value="large">
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeEditDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
};
