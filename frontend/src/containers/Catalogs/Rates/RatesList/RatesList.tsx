import { ChangeEvent, useState } from 'react';
import { Button, Form, Modal, Table } from 'antd';
import { ColumnsType } from 'antd/es/table';
import { EditOutlined, ExclamationCircleOutlined, FileAddOutlined } from '@ant-design/icons';
import { openNotification } from '../../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { editedRates, editRatesPut, IRates } from '../../../../store/services/RatesSlice';
import { EditRates } from '../EditRates/EditRates';

interface Props {
  showDrawerMenu: () => void;
}

export const RatesList = ({ showDrawerMenu }: Props) => {
  const [isEditDrawer, setIsEditDrawer] = useState(false);
  const { ratesList, open, loading } = useAppSelector((state) => state.rates);
  const [editRates, setEditRates] = useState({
    _id: '',
    elementsOfService: '',
    price: 0,
  });
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const showDrawer = (value: any) => {
    setIsEditDrawer(true);
    setEditRates(value);
  };

  const columns: ColumnsType<IRates> = [
    {
      title: 'Элементы услуги',
      dataIndex: 'elementsOfService',
      key: 'elementsOfService',
    },
    {
      title: 'Цена',
      dataIndex: 'price',
      key: 'price',
      render: (_, record) => record.price.toLocaleString('ru'),
    },
    {
      key: 'more',
      title: 'Действие',
      render: (record: IRates) => {
        return (
          <Button onClick={() => showDrawer(record)} type="primary">
            {' '}
            <EditOutlined />
          </Button>
        );
      },
    },
  ];

  const inputEditChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setEditRates((prevState) => ({ ...prevState, [name]: value }));
  };

  const closeEditDrawerHandler = () => {
    setIsEditDrawer(false);
    form.resetFields(['title', '_id', 'headPositionId']);
    setEditRates({
      _id: '',
      elementsOfService: '',
      price: 0,
    });
  };

  const sendEditRatesHandler = () => {
    const onOk = async () => {
      dispatch(editedRates(editRates));
      dispatch(editRatesPut(editRates));
      closeEditDrawerHandler();
      open === false;
    };

    const hideModal = () => {
      open === false;
    };

    Modal.confirm({
      title: 'Предупреждение',
      icon: <ExclamationCircleOutlined />,
      content: 'Вы действительно хотите изменить данный тариф ?',
      okText: 'OK',
      cancelText: 'Отменить',
      open: open,
      onOk: onOk,
      onCancel: hideModal,
    });
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, заполните обязательные поля');
  };
  return (
    <>
      <Button type="primary" onClick={showDrawerMenu} icon={<FileAddOutlined />} size="large">
        Добавить тариф
      </Button>
      <Table
        loading={loading}
        rowKey={(record) => record._id}
        dataSource={ratesList}
        size="small"
        columns={columns}
        style={{ width: '400px', marginTop: '20px' }}
      />
      <Form form={form} />
      <EditRates
        form={form}
        closeEditDrawerHandler={closeEditDrawerHandler}
        isEditDrawer={isEditDrawer}
        sendEditRatesHandler={sendEditRatesHandler}
        onFinishFailed={onFinishFailed}
        editRates={editRates}
        inputEditChangeHandler={inputEditChangeHandler}
      />
    </>
  );
};
