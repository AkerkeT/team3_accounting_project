import { ChangeEvent, useEffect, useState } from 'react';
import { Button, Drawer, Form, FormInstance, Input, Select, Space } from 'antd';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getServiceElementsList } from '../../../../store/services/ServiceElementsSlice';

interface Props {
  closeDrawerHandler?: () => void;
  inputChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  newRates?: {
    elementsOfService: string;
    price: number;
  };
  onFinishFailed?: () => void;
  sendNewRatesHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
  selectHandler?: (value: string) => void;
}

export const CreateNewRates = ({
  closeDrawerHandler,
  isDrawer,
  form,
  sendNewRatesHandler,
  onFinishFailed,
  newRates,
  inputChangeHandler,
  selectHandler,
}: Props) => {
  const { serviceElementsList } = useAppSelector((state) => state.serviceElements);
  const { Option } = Select;
  const dispatch = useAppDispatch();
  const [isDisabled, setIsDisabled] = useState(true);
  const changeFormHandler = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setIsDisabled(hasErrors ? hasErrors : false);
  };

  useEffect(() => {
    dispatch(getServiceElementsList());
  }, [dispatch]);

  return (
    <>
      <Drawer
        title="Добавить новый тариф"
        placement="right"
        onClose={closeDrawerHandler}
        open={isDrawer}
      >
        <Space direction="vertical">
          <Form
            form={form}
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: false }}
            onFinish={sendNewRatesHandler}
            onFinishFailed={onFinishFailed}
            onFieldsChange={changeFormHandler}
            autoComplete="off"
          >
            <label>
              <span style={{ color: 'red' }}>*</span> Элементы услуги:
            </label>
            <Form.Item
              name="elementsOfService"
              rules={[{ required: true, message: 'Пожалуйста, выберите услугу' }]}
            >
              <Select value={newRates?.elementsOfService} onChange={selectHandler}>
                {serviceElementsList.map((service) => (
                  <Option key={service._id} value={service.title}>
                    {service.title}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <label>
              <span style={{ color: 'red' }}>*</span> Цена:
            </label>
            <Form.Item
              name="price"
              rules={[
                { required: true, message: 'Пожалуйста, введите цену для тарифа' },
                {
                  pattern: new RegExp(/^[0-9]{1,}$/),
                  message: 'Цена должен состоять только из цифр',
                },
                {
                  pattern: new RegExp(/^\S/),
                  message: 'поле не должно содержать лишних пробелов',
                },
              ]}
            >
              <Input
                name="price"
                placeholder="Цена"
                value={newRates?.price}
                onChange={inputChangeHandler}
              />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 0 }}>
              <Button
                disabled={isDisabled}
                type="primary"
                htmlType="submit"
                style={{ margin: '15px' }}
                value="large"
              >
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
};
