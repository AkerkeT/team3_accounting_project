import { ChangeEvent, useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { Button, Form, Table } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import {
  editedElement,
  editFetchCurrentServiceElement,
  getServiceElementsList,
  IServiceElements,
} from '../../../../store/services/ServiceElementsSlice';
import { EditCurrentServiceElementFunc } from '../editCurrentServiceElement/EditCurrentServiceElement';
interface ServiceElementsListProps {
  elements: IServiceElements[];
  loading: boolean;
}

export const ServiceElementsList: React.FC<ServiceElementsListProps> = ({ elements, loading }) => {
  const [isEditingDrawer, setIsEditingDrawer] = useState(false);

  const [currentServiceElement, setCurrentServiceElement] = useState({
    _id: '',
    title: '',
  });

  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const { successfully } = useAppSelector((state) => state.serviceElements, shallowEqual);

  const columns = [
    {
      title: 'Наименование',
      dataIndex: 'title',
      key: 'title',
    },

    {
      key: 'more',
      title: 'Действие',
      render: (record: IServiceElements) => {
        return (
          <Button onClick={() => showDrawer(record)} type="primary">
            {' '}
            <EditOutlined />
          </Button>
        );
      },
    },
  ];

  useEffect(() => {
    if (successfully) {
      dispatch(getServiceElementsList());
    }
  }, [dispatch, successfully]);

  const closeEditingDrawerHandler = () => {
    setIsEditingDrawer(false);
    form.resetFields(['title']);
    setCurrentServiceElement({
      _id: '',
      title: '',
    });
  };

  const editingInputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setCurrentServiceElement((prevState) => ({ ...prevState, [name]: value }));
  };

  const sendEditElementHandler = () => {
    dispatch(editFetchCurrentServiceElement(currentServiceElement));
    dispatch(editedElement(currentServiceElement));
    closeEditingDrawerHandler();
  };
  const showDrawer = (value: any) => {
    setIsEditingDrawer(true);
    setCurrentServiceElement(value);
  };
  return (
    <>
      <Table
        loading={loading}
        rowKey={(record) => record._id}
        dataSource={elements}
        columns={columns}
        pagination={{ pageSize: 5 }}
        style={{ width: '400px', marginTop: '20px' }}
      />
      <Form form={form} />
      <EditCurrentServiceElementFunc
        isDrawer={isEditingDrawer}
        editingInputChangeHandler={editingInputChangeHandler}
        closeEditingDrawerHandler={closeEditingDrawerHandler}
        form={form}
        sendEditElementHandler={sendEditElementHandler}
        currentServiceElement={currentServiceElement}
      />
    </>
  );
};
