import { useEffect, useState } from 'react';
import { Button, Form, Input, Select, Space } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getServiceElementsList } from '../../../../store/services/ServiceElementsSlice';

import './ServiceTypeForm.css';

export const ServiceTypeForm = () => {
  const [paymentOption, setPaymentOption] = useState([
    {
      label: '',
      value: '',
    },
  ]);
  const { serviceElementsList } = useAppSelector((state) => state.serviceElements);
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getServiceElementsList());
  }, [dispatch]);

  useEffect(() => {
    const options = serviceElementsList.map((option) => {
      const paymentOption = {
        label: option.title,
        value: option.title,
      };
      return paymentOption;
    });
    setPaymentOption(options);
  }, [serviceElementsList]);
  return (
    <Form.List name="serviceElements">
      {(fields, { add, remove }) => (
        <>
          {fields.map(({ key, name, ...restField }) => (
            <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
              Элемент услуги
              <Form.Item
                {...restField}
                name={[name, 'title']}
                rules={[{ required: true, message: 'Пожалуйста, выберите элемент услуги' }]}
                style={{ width: '160px' }}
              >
                <Select
                  style={{ display: 'block', borderRadius: '9px', width: '160px' }}
                  options={paymentOption}
                />
              </Form.Item>
              Количество
              <Form.Item
                {...restField}
                name={[name, 'quantity']}
                style={{ width: '160px' }}
                rules={[
                  { required: true, message: 'Пожалуйста, введите количество' },
                  {
                    pattern: new RegExp(/^0*?[1-9]\d*$/),
                    message: 'Количество должно состоять только из цифр больше 0',
                  },
                  {
                    pattern: new RegExp(/^\S/),
                    message: 'Поле не должно содержать лишних пробелов',
                  },
                ]}
              >
                <Input name="quantity" />
              </Form.Item>
              Цена
              <Form.Item
                {...restField}
                name={[name, 'price']}
                style={{ width: '200px' }}
                rules={[
                  { required: true, message: 'Пожалуйста, введите цену' },
                  {
                    pattern: new RegExp(/^0*?[1-9]\d*$/),
                    message: 'Цена должна состоять только из цифр больше 0',
                  },
                  {
                    pattern: new RegExp(/^\S/),
                    message: 'Поле не должно содержать лишних пробелов',
                  },
                ]}
              >
                <Input name="price" />
              </Form.Item>
              <MinusCircleOutlined className="minusCircle" onClick={() => remove(name)} />
            </Space>
          ))}
          <Form.Item>
            <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
              Добавить абонентскую плату
            </Button>
          </Form.Item>
        </>
      )}
    </Form.List>
  );
};
