import { useEffect, useState } from 'react';
import { Modal } from 'antd';
import { DatePicker, Form } from 'antd';
import rusLocale from 'antd/es/date-picker/locale/ru_RU';
import moment from 'moment';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import {
  getServicePayment,
  postServicePayment,
  resetServicePayment,
} from '../../../store/services/ServicePaymentSlice';
import { ServiceTypeForm } from './ServiceTypeForm/ServiceTypeForm';

import './AddServicePayment.css';

interface IServiceProps {
  isModalOpen: boolean;
  handleOk: () => void;
  handleCancel: () => void;
  clientId: string | undefined;
}

interface IServiceFormTypes {
  title: string;
  quantity: number;
  price: number;
  sum: number;
}
interface IInputValues {
  startDate: Date;
  clientId: string | undefined;
  serviceElements: IServiceFormTypes[];
  totalSum: number;
}

export const AddServicePayment = ({
  isModalOpen,
  handleOk,
  handleCancel,
  clientId,
}: IServiceProps) => {
  const [disabledSave, setDisabledSave] = useState(true);
  const { message, hasError, addedSuccessfully } = useAppSelector((state) => state.servicePayment);
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  useEffect(() => {
    dispatch(getServicePayment());
  }, []);

  useEffect(() => {
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetServicePayment());
      }, 1000);
    }
    if (addedSuccessfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetServicePayment());
      }, 1000);
    }
  }, [hasError, addedSuccessfully]);

  const formInitialData = {
    startDate: null,
    clientId: '',
    serviceElements: [],
    totalSum: 0,
  };

  const onFinishFailed = () => {
    openNotification('error', 'bottomLeft', 'Не ввели обязательные поля');
  };

  const onFinish = (values: IInputValues) => {
    const valuesCopy = { ...values };
    valuesCopy.startDate = moment(values.startDate).toDate();
    valuesCopy.clientId = clientId;
    if (values.serviceElements.length === 0) {
      openNotification('warning', 'bottomLeft', 'Добавьте абонентскую плату');
      return;
    }
    const serviceElementsCopy = [...valuesCopy.serviceElements];
    serviceElementsCopy.forEach((element) => (element.sum = element.quantity * element.price));
    valuesCopy.serviceElements = serviceElementsCopy;
    const totalSum = valuesCopy.serviceElements
      .map((element: { sum: any }) => element.sum)
      .reduce((acc: any, num: any) => acc + num, 0);
    valuesCopy.totalSum = totalSum;
    dispatch(postServicePayment(valuesCopy)).unwrap();
    handleOk();
    form.resetFields();
    setDisabledSave(true);
  };

  const onCancelSubmit = () => {
    handleCancel();
    form.resetFields();
    setDisabledSave(true);
  };

  const handleFormChange = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setDisabledSave(hasErrors ? hasErrors : false);
  };
  return (
    <>
      <Modal
        title="Добавление абонентской платы в разбивке данных"
        open={isModalOpen}
        onOk={() => form.submit()}
        okText="Сохранить"
        cancelText="Отмена"
        onCancel={onCancelSubmit}
        okButtonProps={{ disabled: disabledSave }}
        width={1000}
      >
        <Form
          form={form}
          layout="vertical"
          autoComplete="off"
          onFinish={onFinish}
          initialValues={formInitialData}
          onFinishFailed={onFinishFailed}
          onFieldsChange={handleFormChange}
          style={{ width: '900px' }}
        >
          <Form.Item
            label="Начала услуги"
            name="startDate"
            rules={[{ required: true, message: 'Пожалуйста, выберите дату' }]}
          >
            <DatePicker format="DD.MM.YYYY" locale={rusLocale} placeholder="Выберите дату" />
          </Form.Item>
          <ServiceTypeForm />
        </Form>
      </Modal>
    </>
  );
};
