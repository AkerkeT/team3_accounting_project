import { ChangeEvent, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import {
  Breadcrumb,
  Button,
  Col,
  Form,
  Modal,
  Row,
  Select,
  SelectProps,
  Tag,
  Typography,
} from 'antd';
import { ExclamationCircleOutlined, SearchOutlined } from '@ant-design/icons';
import type { CustomTagProps } from 'rc-select/lib/BaseSelect';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import {
  addClient,
  deleteClient,
  getClientFilteredStatus,
  getClients,
  getClientsFromEmployee,
  getClientWithoutEmployees,
  IClient,
  resetStatus,
  setCurrentClient,
} from '../../../store/services/ClientsListSlice';
import { clearClientArrayState } from '../../../store/services/ClientsListSlice';
import { ClientAddDrawer } from './ClientAddDrawer/ClientAddDrawer';
import { ClientsTableElements } from './ClientsTableElements/ClientsTableElements';
import NotificationOfAbsentEmployees from './NotificationOfAbsentEmployees/NotificationOfAbsentEmployees';

export const ClientsList = function () {
  const { clients, open, loading } = useAppSelector((state) => state.clients);
  const user = useAppSelector((state) => state.users.user?.user);
  const [individualEntrepreneur, setIsIndividualEntrepreneur] = useState(false);
  const [statesFilter, setStatesFilter] = useState<string[]>([]);
  const [toggle, setToggle] = useState(false);
  const { message, hasError, successfully } = useAppSelector((state) => state.clients);
  const [newClient, setNewClient] = useState({
    bin: '',
    iin: '',
    name: '',
    typeOfEconomicActivity: '',
    status: '',
  });

  useEffect(() => {
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
  }, [successfully, hasError]);

  const { Title } = Typography;
  const [form] = Form.useForm();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    if (user?.roleId === process.env.REACT_APP_MAIN_ROLE) {
      dispatch(getClients());
    } else {
      dispatch(getClientsFromEmployee());
    }
    return () => {
      dispatch(clearClientArrayState());
    };
  }, [dispatch]);

  useEffect(() => {}, [individualEntrepreneur]);

  const [isDrawer, setIsDrawer] = useState(false);

  const closeDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields(['bin', 'iin', 'name', 'typeOfEconomicActivity', 'status']);
    setNewClient({
      bin: '',
      iin: '',
      name: '',
      typeOfEconomicActivity: '',
      status: '',
    });
    setIsIndividualEntrepreneur(false);
  };

  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    if (individualEntrepreneur) {
      form.resetFields(['bin']);
    } else {
      form.resetFields(['iin']);
    }
    setNewClient((prevState) => ({ ...prevState, [name]: value.trim() }));
  };
  const selectChangeHandler = (value: string) => {
    const copy = { ...newClient };
    copy.status = value;
    setNewClient(copy);
  };

  const isIndividualEntrepreneur = () => {
    setIsIndividualEntrepreneur(!individualEntrepreneur);
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, введите обязательные поля');
  };

  const InformationClient = (record: IClient) => {
    dispatch(setCurrentClient(record));
    navigate(`/clients_list/${record?._id}`);
  };

  const deleteHandler = (record: IClient) => {
    const onOk = () => {
      const payload = {
        id: record._id,
        status: 'Удалён',
      };
      dispatch(deleteClient(payload));
      open === false;
    };

    const hideModal = () => {
      open === false;
    };

    return Modal.confirm({
      title: 'Предупреждение',
      icon: <ExclamationCircleOutlined />,
      content: 'Вы действительно хотите удалить клиента?',
      okText: 'OK',
      cancelText: 'Отменить',
      open: open,
      onOk: onOk,
      onCancel: hideModal,
    });
  };

  const sendNewClientHandler = () => {
    const date = new Date();
    const obj = {
      ...newClient,
      registrationDate: date,
      expirationDate: null,
      accompanyingEmployees: [],
      employees: [],
    };
    dispatch(addClient(obj));
    form.resetFields(['bin', 'iin', 'name', 'typeOfEconomicActivity', 'status']);
    closeDrawerHandler();
  };

  const filterChange = (value: string[]) => {
    setStatesFilter(value);
  };

  const filterClickHandler = () => {
    dispatch(getClientFilteredStatus(statesFilter));
    return;
  };

  const options: SelectProps['options'] = [];
  options.push(
    {
      value: 'Активный',
      label: 'Активный',
    },
    {
      value: 'Неактивный',
      label: 'Неактивный',
    },
    {
      value: 'Удалён',
      label: 'Удалён',
    }
  );

  const tagRender = (props: CustomTagProps) => {
    const { label, closable, onClose } = props;

    const onPreventMouseDown = (event: React.MouseEvent<HTMLSpanElement>) => {
      event.preventDefault();
      event.stopPropagation();
    };

    return (
      <Tag
        color="red"
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{ marginRight: 3 }}
      >
        {label}
      </Tag>
    );
  };

  const checkEmployeesOfAllClients = (clients: Array<IClient>) => {
    let counter = 0;
    const clientsWithNoEmployees: IClient[] = [];
    clients.map((client) => {
      if (client.accompanyingEmployees.length === 0) {
        counter++;
        clientsWithNoEmployees.push(client);
      } else {
        return;
      }
    });
    return { counter, clientsWithNoEmployees };
  };

  const data = checkEmployeesOfAllClients(clients);

  const clickIconHandler = () => {
    if (toggle) {
      setToggle(false);
      dispatch(getClients());
    } else {
      dispatch(getClientWithoutEmployees(data.clientsWithNoEmployees));
      setToggle(true);
    }
  };

  return (
    <>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Клиенты</Breadcrumb.Item>
      </Breadcrumb>
      <span style={{ display: 'flex', justifyContent: 'start' }}>
        <Title style={{ marginRight: '60px' }}>Клиенты</Title>
        {data.counter > 0 ? (
          <NotificationOfAbsentEmployees
            clientsNumber={data.counter}
            onClickHandler={clickIconHandler}
          />
        ) : null}
      </span>
      {user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
        <>
          <Select
            mode="tags"
            tagRender={tagRender}
            style={{ width: '25%' }}
            placeholder="Поиск по статусу"
            options={options}
            onChange={filterChange}
          />
          <Button
            style={{ marginLeft: '15px' }}
            onClick={() => filterClickHandler()}
            htmlType="submit"
            type="primary"
            shape="circle"
            icon={<SearchOutlined />}
          />
        </>
      ) : null}
      <Row>
        <Col xs={24} md={{ span: 20, offset: 1 }}>
          <Col style={{ display: 'flex', justifyContent: 'end' }}>
            {user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
              <Button
                style={{ marginBottom: 20 }}
                onClick={showDrawerMenu}
                value="large"
                type="primary"
              >
                Добавить клиента
              </Button>
            ) : null}
          </Col>
          <ClientsTableElements
            deleteHandler={deleteHandler}
            data={clients}
            showDrawer={InformationClient}
            spinner={loading}
          />
        </Col>
        <ClientAddDrawer
          isDrawer={isDrawer}
          form={form}
          isIndividualEntrepreneur={isIndividualEntrepreneur}
          individualEntrepreneur={individualEntrepreneur}
          closeDrawerHandler={closeDrawerHandler}
          sendNewClientHandler={sendNewClientHandler}
          onFinishFailed={onFinishFailed}
          newClient={newClient}
          inputChangeHandler={inputChangeHandler}
          selecthangeHandler={selectChangeHandler}
        />
      </Row>
    </>
  );
};
