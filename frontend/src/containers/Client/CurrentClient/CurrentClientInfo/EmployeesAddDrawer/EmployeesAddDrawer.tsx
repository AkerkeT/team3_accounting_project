import { useEffect, useState } from 'react';
import { Button, Drawer, Form, Select, Space } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { useAppDispatch, useAppSelector } from '../../../../../hooks';
import {
  getEmployeesByPositionId,
  IEmployee,
} from '../../../../../store/services/EmployeesListSlice';
import { getPositions } from '../../../../../store/services/PositionSlice';

interface Props {
  closeDrawerHandler?: () => void;
  accompanyingEmployee?: string;
  selectAccompanyingEmployeeHandler?: (value: string) => void;
  onFinishFailed?: () => void;
  sendAccompanyingEmployeeHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
  value?: string;
}

export const EmployeesAddDrawer = ({
  closeDrawerHandler,
  selectAccompanyingEmployeeHandler,
  onFinishFailed,
  sendAccompanyingEmployeeHandler,
  form,
  isDrawer,
  value,
}: Props) => {
  const { positions } = useAppSelector((state) => state.positions);
  const { employeesFiltered } = useAppSelector((state) => state.employees);
  const { currentClient } = useAppSelector((state) => state.clients);
  const user = useAppSelector((state) => state.users.user?.user);

  const [disabledSave, setDisabledSave] = useState(true);
  const [position, setPosition] = useState('');

  const handleFormChange = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setDisabledSave(hasErrors ? hasErrors : false);
  };

  const selectPositionHandler = (value: string) => {
    setPosition(value);
    form?.resetFields(['employeeName']);
  };

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getPositions());
  }, [dispatch]);

  useEffect(() => {
    position ? dispatch(getEmployeesByPositionId(position)) : null;
  }, [dispatch, position]);

  return (
    <Drawer
      title="Добавление сопровождающего сотрудника"
      placement="right"
      onClose={closeDrawerHandler}
      open={isDrawer}
    >
      <Space direction="vertical">
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: false }}
          onFinish={sendAccompanyingEmployeeHandler}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          onFieldsChange={handleFormChange}
        >
          <Form.Item
            label="Должность"
            name="position"
            rules={[{ required: true, message: 'Пожалуйста, выберите должность' }]}
          >
            <Select
              style={{ display: 'block', borderRadius: '9px' }}
              onChange={selectPositionHandler}
            >
              {user?.positionId?._id === process.env.REACT_APP_LEAD_POSITION
                ? positions
                    ?.filter((item) => item.headPositionId)
                    .map((position) => (
                      <Select.Option key={position._id} value={position._id}>
                        {position.title}
                      </Select.Option>
                    ))
                : positions.map((position) => (
                    <Select.Option key={position._id} value={position._id}>
                      {position.title}
                    </Select.Option>
                  ))}
            </Select>
          </Form.Item>
          <Form.Item
            label="ФИО"
            name="employeeName"
            rules={[{ required: true, message: 'Пожалуйста, выберите ФИО сотрудника' }]}
          >
            <Select
              style={{ display: 'block', borderRadius: '9px' }}
              value={value}
              onChange={selectAccompanyingEmployeeHandler}
            >
              {currentClient?.accompanyingEmployees?.length && position
                ? employeesFiltered
                    .filter((emp) =>
                      currentClient.accompanyingEmployees.every((item) => item._id !== emp._id)
                    )
                    .map((employee: IEmployee) => (
                      <Select.Option title={employee._id} key={employee._id} value={employee._id}>
                        {employee.firstName + ' ' + employee.lastName}
                      </Select.Option>
                    ))
                : employeesFiltered.map((employee: IEmployee) => (
                    <Select.Option title={employee._id} key={employee._id} value={employee._id}>
                      {employee.firstName + ' ' + employee.lastName}
                    </Select.Option>
                  ))}
            </Select>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 0 }}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ margin: '15px' }}
              disabled={disabledSave}
              value="large"
            >
              Сохранить
            </Button>
            <Button
              style={{ margin: '15px' }}
              onClick={closeDrawerHandler}
              value="large"
              type="primary"
              danger
            >
              Отмена
            </Button>
          </Form.Item>
        </Form>
      </Space>
    </Drawer>
  );
};
