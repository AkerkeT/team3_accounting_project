import { ChangeEvent, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Breadcrumb, Button, DatePickerProps, Form, Modal, Tabs } from 'antd';
import { EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { openNotification } from '../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../hooks';
import {
  editCurrentClient,
  getCurrentClient,
  resetStatus,
} from '../../../store/services/ClientsListSlice';
import { ServicePaymentHistory } from '../ServicePaymentHistory/ServicePaymentHistory ';
import { CurrentClientEdit } from './CurrentClientEdit/CurrentClientEdit';
import { CurrentClientInfo } from './CurrentClientInfo/CurrentClientInfo';

export const CurrentClient = () => {
  const dispatch = useAppDispatch();
  const params = useParams();
  const [form] = Form.useForm();
  const [isDrawer, setIsDrawer] = useState(false);
  const { currentClient, open, message, hasError, successfully } = useAppSelector(
    (state) => state.clients
  );

  const user = useAppSelector((state) => state.users.user?.user);

  const [editClient, setEditClient] = useState({
    bin: currentClient?.bin,
    iin: currentClient?.iin,
    name: currentClient?.name,
    typeOfEconomicActivity: currentClient?.typeOfEconomicActivity,
    status: currentClient?.status,
    registrationDate: currentClient?.registrationDate,
    expirationDate: currentClient?.expirationDate,
  });

  useEffect(() => {
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetStatus());
      }, 1000);
    }
  }, [successfully, hasError]);

  useEffect(() => {
    dispatch(getCurrentClient(params.id));
  }, [dispatch]);

  const items = [
    {
      label: 'Информация',
      key: 'item-1',
      children: <CurrentClientInfo />,
    },
    {
      label: 'История',
      key: 'item-3',
      children: <ServicePaymentHistory />,
    },
  ];

  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const closeDrawerHandler = () => {
    setIsDrawer(false);
  };

  const sendEditClientHandler = () => {
    const onOk = async () => {
      const payloadEdit = {
        id: params.id,
        data: {
          status: editClient.status,
          bin: editClient.bin,
          iin: editClient.iin,
          name: editClient.name,
          typeOfEconomicActivity: editClient.typeOfEconomicActivity,
          registrationDate: editClient.registrationDate,
          expirationDate: editClient.expirationDate,
        },
      };
      await dispatch(editCurrentClient(payloadEdit));
      await dispatch(getCurrentClient(params.id));
      open === false;
      closeDrawerHandler();
    };

    const hideModal = () => {
      open === false;
    };

    Modal.confirm({
      title: 'Предупреждение',
      icon: <ExclamationCircleOutlined />,
      content: 'Вы действительно хотите изменить данные клиента?',
      okText: 'OK',
      cancelText: 'Отменить',
      open: open,
      onOk: onOk,
      onCancel: hideModal,
    });
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, заполните обязательные поля!');
  };

  const inputChangeHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event?.target;
    setEditClient({ ...editClient, [name]: value.trim() });
  };

  const selectChangeHandler = (value: string) => {
    const copy = { ...editClient };
    copy.status = value;
    setEditClient(copy);
  };

  const onDateChange: DatePickerProps['onChange'] = (date) => {
    const copy = { ...editClient };
    copy.expirationDate = date ? date.toDate() : new Date();
    setEditClient(copy);
  };

  const onRegDateChange: DatePickerProps['onChange'] = (date) => {
    const copy = { ...editClient };
    copy.registrationDate = date ? date.toDate() : new Date();
    setEditClient(copy);
  };

  return (
    <>
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link to="/">Главная</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <Link to="/clients_list">Клиенты</Link>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Информация о клиенте</Breadcrumb.Item>
        </Breadcrumb>
        {user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
          <>
            {currentClient?.status !== 'Удалён' ? (
              <Button
                onClick={showDrawerMenu}
                type="primary"
                icon={<EditOutlined />}
                size="large"
              />
            ) : null}
          </>
        ) : null}
      </div>
      <Tabs defaultActiveKey="1" items={items} />
      <CurrentClientEdit
        closeDrawerHandler={closeDrawerHandler}
        isDrawer={isDrawer}
        form={form}
        sendEditClientHandler={sendEditClientHandler}
        onFinishFailed={onFinishFailed}
        inputChangeHandler={inputChangeHandler}
        selecthangeHandler={selectChangeHandler}
        currentClient={currentClient}
        onDateChange={onDateChange}
        onRegDateChange={onRegDateChange}
      />
    </>
  );
};
