import { ChangeEvent, useEffect, useState } from 'react';
import { Button, DatePicker, DatePickerProps, Drawer, Form, Input, Select } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getEmployeesWorks } from '../../../../store/services/EmployeesListSlice';
import { getServiceElementsList } from '../../../../store/services/ServiceElementsSlice';

interface Props {
  closeDrawerHandler?: () => void;
  onDateChange?: DatePickerProps['onChange'];
  onEndDateChange?: DatePickerProps['onChange'];
  onPayDateChange?: DatePickerProps['onChange'];
  inputChangeHandler?: (
    e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>
  ) => void;
  newHocService?: {
    serviceElements: string;
    startDate: Date | null;
    endDate?: Date | null;
    payDate?: Date | null;
    employeeId: string[];
    description: string | undefined;
    sum: number;
    clientId: string | undefined;
  };
  onFinishFailed?: () => void;
  sendNewHocService?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
  selectChangeHandler: (value: string) => void;
  selectServiceChangeHandler: (value: string) => void;
}

const { TextArea } = Input;

export const ClientAddHocService = ({
  closeDrawerHandler,
  isDrawer,
  form,
  sendNewHocService,
  onFinishFailed,
  newHocService,
  inputChangeHandler,
  onDateChange,
  onEndDateChange,
  onPayDateChange,
  selectChangeHandler,
  selectServiceChangeHandler,
}: Props) => {
  const { Option } = Select;
  const [isDisabled, setIsDisabled] = useState(true);
  const dispatch = useAppDispatch();
  const { employeesWorks } = useAppSelector((state) => state.employees);
  const { serviceElementsList } = useAppSelector((state) => state.serviceElements);
  const user = useAppSelector((state) => state.users.user?.user);

  useEffect(() => {
    if (user?.roleId === process.env.REACT_APP_MAIN_ROLE) {
      dispatch(getEmployeesWorks());
    }
    dispatch(getServiceElementsList());
  }, [dispatch]);

  const changeFormHandler = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setIsDisabled(hasErrors ? hasErrors : false);
  };

  return (
    <Drawer
      title="Добавить разовую услугу"
      placement="right"
      onClose={closeDrawerHandler}
      open={isDrawer}
    >
      <Form
        form={form}
        name="basic"
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: false }}
        onFinish={sendNewHocService}
        onFinishFailed={onFinishFailed}
        onFieldsChange={changeFormHandler}
        autoComplete="off"
      >
        <Form.Item
          label="Элемент услуги"
          name="serviceElements"
          rules={[
            {
              required: true,
              message: 'Пожалуйста, введите элемент услуги',
            },
            {
              pattern: new RegExp(/^\S/),
              message: 'поле не должно содержать лишних пробелов',
            },
          ]}
        >
          <Select
            style={{ display: 'block', borderRadius: '9px' }}
            value={newHocService?.serviceElements}
            onChange={selectServiceChangeHandler}
          >
            {serviceElementsList.map((date) => (
              <Option key={date.title} children={date.title}></Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Дата начала"
          name="startDate"
          rules={[
            { required: true, message: 'Пожалуйста, введите дату' },
            {
              pattern: new RegExp(/^\S/),
              message: 'поле не должно содержать лишних пробелов',
            },
          ]}
        >
          <DatePicker format={'DD.MM.YYYY'} onChange={onDateChange} />
        </Form.Item>
        <Form.Item
          label="Дата окончания"
          name="endDate"
          rules={[
            {
              pattern: new RegExp(/^\S/),
              message: 'поле не должно содержать лишних пробелов',
            },
          ]}
        >
          <DatePicker format={'DD.MM.YYYY'} onChange={onEndDateChange} />
        </Form.Item>
        <Form.Item
          label="Дата расчета"
          name="payDate"
          rules={[
            {
              pattern: new RegExp(/^\S/),
              message: 'поле не должно содержать лишних пробелов',
            },
          ]}
        >
          <DatePicker format={'DD.MM.YYYY'} onChange={onPayDateChange} />
        </Form.Item>
        <Form.Item
          label="Сотрудник"
          name="employeeId"
          rules={[{ required: true, message: 'Пожалуйста, выберите сотрудника' }]}
        >
          <Select
            placeholder="Выберите сотрудника"
            mode="multiple"
            style={{ display: 'block', borderRadius: '9px' }}
            onChange={selectChangeHandler}
          >
            {employeesWorks.map((works) => (
              <Option key={works._id} children={`${works.firstName} ${works.lastName}`} />
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          label="Описание"
          name="description"
          rules={[
            {
              pattern: new RegExp(/^\S/),
              message: 'поле не должно содержать лишних пробелов',
            },
          ]}
        >
          <TextArea
            value={newHocService?.description}
            placeholder="Описание"
            name="description"
            autoSize={{ minRows: 3, maxRows: 8 }}
            onChange={inputChangeHandler}
          />
        </Form.Item>
        <Form.Item
          label="Сумма"
          name="sum"
          rules={[
            {
              required: true,
              message: 'Пожалуйста, введите сумму',
            },
            {
              pattern: new RegExp(/^\S/),
              message: 'поле не должно содержать лишних пробелов',
            },
            {
              pattern: new RegExp(/^[0-9]{1,}$/),
              message: 'сумма должна состоять только из цифр',
            },
          ]}
        >
          <Input value={newHocService?.sum} name="sum" onChange={inputChangeHandler} />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 0 }}>
          <Button
            disabled={isDisabled}
            type="primary"
            htmlType="submit"
            style={{ margin: '15px' }}
            value="large"
          >
            Сохранить
          </Button>
          <Button
            style={{ margin: '15px' }}
            onClick={closeDrawerHandler}
            value="large"
            type="primary"
            danger
          >
            Отмена
          </Button>
        </Form.Item>
      </Form>
    </Drawer>
  );
};
