import { Link } from 'react-router-dom';
import { Breadcrumb, Button, Progress, Typography } from 'antd';
import Table from 'antd/lib/table';

interface IData {
  id: number;
  employeeName: string;
  position: string;
  status: string;
  dateOfCreation: string;
  kpiPercent: number;
}

const mockData = [
  {
    id: 1,
    employeeName: 'Шамшура Надежда',
    position: 'Бухгалтер',
    status: 'Согласован',
    dateOfCreation: '20.01.2020',
    kpiPercent: 80,
  },
  {
    id: 2,
    employeeName: 'Шамшура Надежда',
    position: 'Бухгалтер',
    status: 'Согласован',
    dateOfCreation: '20.01.2020',
    kpiPercent: 67,
  },
  {
    id: 3,
    employeeName: 'Шамшура Надежда',
    position: 'Бухгалтер',
    status: 'Согласован',
    dateOfCreation: '20.01.2020',
    kpiPercent: 37,
  },
  {
    id: 4,
    employeeName: 'Шамшура Надежда',
    position: 'Бухгалтер',
    status: 'Согласован',
    dateOfCreation: '20.01.2020',
    kpiPercent: 100,
  },
];

export const MonthlyKPIAgreement = () => {
  const { Title } = Typography;
  const columns = [
    {
      title: '№',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'ФИО сотрудника',
      dataIndex: 'employeeName',
      key: 'month',
    },
    {
      title: 'Должность',
      dataIndex: 'position',
      key: 'position',
    },
    {
      title: 'Статус',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Дата создания',
      dataIndex: 'dateOfCreation',
      key: 'dateOfCreation',
    },
    {
      title: '% KPI',
      key: 'kpiPercent',
      render: (record: IData) => {
        return <Progress style={{ width: 150 }} percent={record.kpiPercent} size="small" />;
      },
    },
    {
      title: 'Действие',
      key: 'action',
      render: () => {
        return <Button type="primary"> Подробнее</Button>;
      },
    },
  ];
  return (
    <div>
      <Breadcrumb>
        <Breadcrumb.Item>
          <Link to="/">Главная</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to="/kpi_agreements">Согласование</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>На согласовании</Breadcrumb.Item>
      </Breadcrumb>
      <Title>На согласовании</Title>
      <Button style={{ margin: '20px', marginTop: 0 }} type="primary">
        Закрыть месяц
      </Button>

      <Table
        dataSource={mockData}
        columns={columns}
        size="middle"
        rowKey={(record) => record.id + Math.random() + Math.random()}
        pagination={false}
      />
    </div>
  );
};
