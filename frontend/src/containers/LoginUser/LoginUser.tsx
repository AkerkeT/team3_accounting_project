import React, { ChangeEvent, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Button, Form, Input, Modal, Row } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { UserOutlined } from '@ant-design/icons';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { loginUser } from '../../store/services/UsersSlice';

export const LoginUser: React.FC = () => {
  const [form] = useForm();
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [disabledSave, setDisabledSave] = useState(false);
  const { loginError } = useAppSelector((state) => state.users);

  const [userForm, setUserForm] = useState({
    login: '',
    password: '',
  });

  const error = () => {
    Modal.error({
      title: 'Ошибка',
      content: loginError?.message,
    });
  };

  useEffect(() => {
    if (loginError) error();
  }, [loginError]);

  const handleFormChange = () => {
    const hasErrors = form.getFieldsError().some(({ errors }) => errors.length);
    setDisabledSave(hasErrors);
  };

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setUserForm((prevState) => ({ ...prevState, [name]: value.trim() }));
  };

  const submitFormHandler = () => {
    dispatch(
      loginUser({
        userData: { ...userForm },
        navigate,
      })
    );
  };

  return (
    <Row
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <UserOutlined style={{ fontSize: 50, color: 'rgb(29 111 186)', marginTop: '10%' }} />
      <Form
        form={form}
        onFieldsChange={handleFormChange}
        style={{ width: '30%', marginTop: '1%', marginRight: '10%' }}
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: false }}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="login"
          rules={[
            { required: true, message: 'Пожалуйста, введите login' },
            {
              pattern: new RegExp(
                /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu
              ),
              message: 'Укажите пожалуйста корректный email',
            },
          ]}
        >
          <Input value={userForm.login} name="login" onChange={inputChangeHandler} />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[
            { required: true, message: 'Пожалуйста введите пароль' },
            {
              pattern: new RegExp(/^\S/),
              message: 'Поле не должно содержать лишних пробелов',
            },
          ]}
        >
          <Input.Password value={userForm.password} name="password" onChange={inputChangeHandler} />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button
            disabled={disabledSave}
            onClick={submitFormHandler}
            type="primary"
            htmlType="submit"
          >
            Войти
          </Button>
        </Form.Item>
      </Form>
    </Row>
  );
};
