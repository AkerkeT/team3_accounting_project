import { ChangeEvent, useState } from 'react';
import { Button, Drawer, Form, FormInstance, Input, Space } from 'antd';

interface Props {
  closeAccountDrawerHandler?: () => void;
  inputAccountChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  newAccount?: {
    login: string;
    password: string;
  };
  onFinishFailed?: () => void;
  sendNewAccountHandler?: () => void;
  form?: FormInstance;
  isAccountDrawer?: boolean;
}

export function CurrentCreateAccount({
  closeAccountDrawerHandler,
  isAccountDrawer,
  form,
  sendNewAccountHandler,
  onFinishFailed,
  newAccount,
  inputAccountChangeHandler,
}: Props) {
  const [isDisabled, setIsDisabled] = useState(true);
  const changeFormHandler = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setIsDisabled(hasErrors ? hasErrors : false);
  };

  return (
    <>
      <Drawer
        title="Создать аккаунт для сотрудника"
        placement="right"
        onClose={closeAccountDrawerHandler}
        open={isAccountDrawer}
      >
        <Space direction="vertical">
          <Form
            form={form}
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: false }}
            onFinish={sendNewAccountHandler}
            onFinishFailed={onFinishFailed}
            onFieldsChange={changeFormHandler}
            autoComplete="off"
          >
            <label>
              <span style={{ color: 'red' }}>*</span> Логин:
            </label>
            <Form.Item
              name="login"
              rules={[{ required: true, message: 'Пожалуйста, введите навание логина' }]}
            >
              <Input
                type="email"
                name="login"
                placeholder="Логин"
                value={newAccount?.login}
                onChange={inputAccountChangeHandler}
              />
            </Form.Item>
            <label>
              <span style={{ color: 'red' }}>*</span> Пароль:
            </label>
            <Form.Item
              name="password"
              rules={[{ required: true, message: 'Пожалуйста, придумайте пароль' }]}
            >
              <Input
                minLength={8}
                name="password"
                placeholder="Пароль"
                value={newAccount?.password}
                onChange={inputAccountChangeHandler}
              />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 0 }}>
              <Button
                disabled={isDisabled}
                type="primary"
                htmlType="submit"
                style={{ margin: '15px' }}
                value="large"
              >
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeAccountDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
}
