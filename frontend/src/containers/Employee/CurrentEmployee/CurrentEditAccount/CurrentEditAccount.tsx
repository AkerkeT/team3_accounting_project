import { ChangeEvent, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Button, Drawer, Form, FormInstance, Input, Space } from 'antd';
import { useAppDispatch } from '../../../../hooks';
import { getUserEmployeeId } from '../../../../store/services/RegisterUserSlice';

interface Props {
  closeEditAccountDrawerHandler?: () => void;
  inputEditAccountChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  editAccount?: {
    login: string | undefined;
    password: string;
  };
  onEditFinishFailed?: () => void;
  sendEditAccountHandler?: () => void;
  formInstance?: FormInstance;
  isEditAccountDrawer?: boolean;
}

export function CurrentEditAccount({
  closeEditAccountDrawerHandler,
  isEditAccountDrawer,
  formInstance,
  sendEditAccountHandler,
  onEditFinishFailed,
  editAccount,
  inputEditAccountChangeHandler,
}: Props) {
  const dispatch = useAppDispatch();
  const params = useParams();

  useEffect(() => {
    const payload = {
      id: params.id,
    };
    dispatch(getUserEmployeeId(payload));
  }, [dispatch]);

  return (
    <>
      <Drawer
        title="Редактировать аккаунт для сотрудника"
        placement="right"
        onClose={closeEditAccountDrawerHandler}
        open={isEditAccountDrawer}
      >
        <Space direction="vertical">
          <Form
            form={formInstance}
            name="basic"
            labelCol={{ span: 2 }}
            wrapperCol={{ span: 20 }}
            initialValues={{ remember: false }}
            onFinish={sendEditAccountHandler}
            onFinishFailed={onEditFinishFailed}
            autoComplete="off"
          >
            <label>
              <span style={{ color: 'red' }}>*</span> Логин:
            </label>
            <Form.Item
              name="login"
              rules={[{ required: true, message: 'Пожалуйста, введите логин' }]}
              initialValue={editAccount?.login}
            >
              <Input
                type="email"
                name="login"
                placeholder="Логин"
                disabled
                onChange={inputEditAccountChangeHandler}
              />
            </Form.Item>
            <label>
              <span style={{ color: 'red' }}>*</span> Пароль:
            </label>
            <Form.Item
              name="password"
              rules={[{ required: true, message: 'Пожалуйста, придумайте пароль' }]}
            >
              <Input
                name="password"
                placeholder="Пароль"
                minLength={8}
                onChange={inputEditAccountChangeHandler}
                value={editAccount?.password}
              />
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 0 }}>
              <Button type="primary" htmlType="submit" style={{ margin: '15px' }} value="large">
                Сохранить
              </Button>
              <Button
                style={{ margin: '15px' }}
                onClick={closeEditAccountDrawerHandler}
                value="large"
                type="primary"
                danger
              >
                Отмена
              </Button>
            </Form.Item>
          </Form>
        </Space>
      </Drawer>
    </>
  );
}
