import { ChangeEvent, useEffect, useState } from 'react';
import { shallowEqual } from 'react-redux';
import { useParams } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { Button, Col, Collapse, Descriptions, Form, Row, Spin, Table, Typography } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import { openNotification } from '../../../../components/helpers/helpers';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getClients, IClient, setCurrentClient } from '../../../../store/services/ClientsListSlice';
import { getClientsOfEmployee } from '../../../../store/services/EmployeesListSlice';
import { getUserEmployeeId, registerUser } from '../../../../store/services/RegisterUserSlice';
import { resetMessage } from '../../../../store/services/RegisterUserSlice';
import { CurrentAccountList } from '../CurrentAccountList/CurrentAccountList';
import { CurrentCreateAccount } from '../CurrentCreateAccount/CurrentCreateAccount';

import './CurrentEmployeeInfo.css';

const { Panel } = Collapse;
const { Title } = Typography;

export const CurrentEmployeeInfo = () => {
  const dispatch = useAppDispatch();
  const [isDrawer, setIsDrawer] = useState(false);
  const [isEditDrawer, setEditIsDrawer] = useState(false);
  const [form] = Form.useForm();
  const params = useParams();
  const [AccountState, setAccountState] = useState({
    login: '',
    password: '',
  });
  const user = useAppSelector((state) => state.users.user?.user);

  const { currentEmployee, clientsOfCurrentEmployee, loading } = useAppSelector(
    (state) => state.employees,
    shallowEqual
  );
  const { message, successfully, hasError, userData, loadingReg } = useAppSelector(
    (state) => state.userRegister,
    shallowEqual
  );

  const navigate = useNavigate();

  const InformationClient = (record: IClient) => {
    dispatch(setCurrentClient(record));
    navigate(`/clients_list/${record?._id}`);
  };

  useEffect(() => {
    const payload = {
      id: params.id,
    };
    dispatch(getUserEmployeeId(payload));
  }, [dispatch]);

  useEffect(() => {
    if (successfully) {
      openNotification('success', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetMessage());
      }, 1000);
    }
    if (hasError) {
      openNotification('error', 'bottomLeft', message);
      setTimeout(() => {
        dispatch(resetMessage());
      }, 1000);
    }
  }, [successfully, hasError]);

  const showEditDrawer = () => {
    setEditIsDrawer(true);
  };

  useEffect(() => {
    if (currentEmployee) {
      dispatch(getClientsOfEmployee(currentEmployee?._id));
    }
  }, [dispatch, currentEmployee?._id]);

  useEffect(() => {
    dispatch(getClients());
  }, [dispatch]);

  const startDate = currentEmployee?.dateOfEmployment;
  const endDate = currentEmployee?.dateOfDismissal;

  const showDrawerMenu = () => {
    setIsDrawer(true);
  };

  const columnsClients: ColumnsType<IClient> = [
    {
      title: 'БИН/ИИН',
      dataIndex: 'bin' || 'iin',
      key: 'bin' || 'iin',
      render: (_, record) => {
        return record.bin ? record.bin : record.iin;
      },
    },
    {
      title: 'Наименование',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Вид деятельности',
      dataIndex: 'typeOfEconomicActivity',
      key: 'typeOfEconomicActivity',
    },
    {
      key: 'more',
      title: 'Действие',
      render: (_, record) => {
        return (
          <Button onClick={() => InformationClient(record)} type="primary">
            {' '}
            Подробнее
          </Button>
        );
      },
    },
  ];

  const closeAccountDrawerHandler = () => {
    setIsDrawer(false);
    form.resetFields(['login', 'password']);
    setAccountState({
      login: '',
      password: '',
    });
  };

  const inputChangeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setAccountState((prevState) => ({ ...prevState, [name]: value.trim() }));
  };

  const sendNewAccountHandler = () => {
    const data = {
      login: AccountState.login,
      password: AccountState.password,
      employeeId: currentEmployee?._id,
      positionId: currentEmployee?.position._id,
    };
    dispatch(registerUser(data));
    const payload = {
      id: params.id,
    };
    dispatch(getUserEmployeeId(payload));
    closeAccountDrawerHandler();
  };

  const onFinishFailed = () => {
    openNotification('warning', 'bottomLeft', 'Пожалуйста, введите обязательные поля');
  };

  return (
    <>
      <Title level={2}>Информация о сотруднике</Title>
      <Row justify="space-evenly">
        <Col span={4}>
          <strong>ФИО:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>{`${currentEmployee?.firstName} ${currentEmployee?.lastName}`}</Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={5}>
          <strong>Должность:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>{currentEmployee?.position?.title}</Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={3}>
          <strong>Фикс. оклад:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>
              {currentEmployee?.fixedSalary.toLocaleString('ru')}
            </Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={3}>
          <strong>Дата принятия:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>
              {new Date(String(startDate)).toLocaleDateString()}
            </Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={3}>
          <strong>Дата увольнения:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>
              {String(endDate) === 'null' ? ' ' : new Date(String(endDate)).toLocaleDateString()}
            </Descriptions.Item>
          </Descriptions>
        </Col>
        <Col span={2}>
          <strong>Статус:</strong>
          <Descriptions style={{ fontSize: '16px' }}>
            <Descriptions.Item>{currentEmployee?.status}</Descriptions.Item>
          </Descriptions>
        </Col>
      </Row>
      <Collapse defaultActiveKey={['1']}>
        <Panel header="Клиенты на сопровождении" key="1">
          {clientsOfCurrentEmployee?.length ? (
            <Table
              loading={loading}
              dataSource={clientsOfCurrentEmployee}
              rowKey={() => Math.random() + Math.random()}
              columns={columnsClients}
            />
          ) : (
            <p>Добавить клиента на сопровождение</p>
          )}
          <Button type="primary" size="large">
            Добавить
          </Button>
        </Panel>
        {user?.roleId === process.env.REACT_APP_MAIN_ROLE ? (
          <Panel header="Доступ в систему" key="2">
            {loadingReg ? (
              <Spin />
            ) : userData ? (
              <Descriptions layout="vertical">
                <Descriptions.Item label="Логин" style={{ fontWeight: 'bold' }}>
                  {userData.login}
                </Descriptions.Item>
                {/* <Descriptions.Item label="Пароль" style={{ fontWeight: 'bold' }}>
                {userData.password.replace(/[\s\S]/g, '*')}
              </Descriptions.Item> */}
              </Descriptions>
            ) : (
              <p>Отсутствует доступ в систему</p>
            )}

            {userData ? (
              <Button type="primary" onClick={showEditDrawer} size="large">
                Редактировать
              </Button>
            ) : (
              <Button type="primary" onClick={showDrawerMenu} size="large">
                Создать аккаунт
              </Button>
            )}
          </Panel>
        ) : null}
      </Collapse>
      <CurrentCreateAccount
        isAccountDrawer={isDrawer}
        closeAccountDrawerHandler={closeAccountDrawerHandler}
        form={form}
        sendNewAccountHandler={sendNewAccountHandler}
        onFinishFailed={onFinishFailed}
        newAccount={AccountState}
        inputAccountChangeHandler={inputChangeHandler}
      />
      <CurrentAccountList isEditDrawer={isEditDrawer} setIsDrawer={setEditIsDrawer} />
    </>
  );
};
