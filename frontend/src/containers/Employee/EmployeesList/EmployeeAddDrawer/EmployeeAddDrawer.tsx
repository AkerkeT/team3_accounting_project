import { ChangeEvent, useEffect, useState } from 'react';
import { Button, DatePicker, DatePickerProps, Drawer, Form, Input, Select, Space } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { useAppDispatch, useAppSelector } from '../../../../hooks';
import { getEmployeesByPositionId } from '../../../../store/services/EmployeesListSlice';
import {
  getPositionById,
  getPositions,
  resetPosition,
} from '../../../../store/services/PositionSlice';

interface Props {
  closeDrawerHandler?: () => void;
  selectStatusHandler?: (value: string) => void;
  onDateChange?: DatePickerProps['onChange'];
  inputChangeHandler?: (e: ChangeEvent<HTMLInputElement>) => void;
  newEmployee?: {
    firstName: string;
    lastName: string;
    position: string;
    status: string;
    fixedSalary: number;
    headId: string | null;
    dateOfDismissal: null;
    dateOfEmployment: Date;
  };
  selectPositionHandler?: (value: string) => void;
  selectHeadIdHandler?: (value: string) => void;
  onFinishFailed?: () => void;
  sendNewEmployeeHandler?: () => void;
  form?: FormInstance;
  isDrawer?: boolean;
}

export const EmployeeAddDrawer = ({
  closeDrawerHandler,
  selectStatusHandler,
  onDateChange,
  inputChangeHandler,
  newEmployee,
  selectPositionHandler,
  selectHeadIdHandler,
  onFinishFailed,
  sendNewEmployeeHandler,
  form,
  isDrawer,
}: Props) => {
  const { Option } = Select;
  const { positions, position } = useAppSelector((state) => state.positions);
  const { employeesFiltered } = useAppSelector((state) => state.employees);
  const user = useAppSelector((state) => state.users.user?.user);

  const [disabledSave, setDisabledSave] = useState(true);

  const handleFormChange = () => {
    const hasErrors = form?.getFieldsError().some(({ errors }) => errors.length);
    setDisabledSave(hasErrors ? hasErrors : false);
  };

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getPositions());
  }, [dispatch]);

  useEffect(() => {
    newEmployee?.position ? dispatch(getPositionById(newEmployee?.position)) : null;
    return () => {
      dispatch(resetPosition());
    };
  }, [newEmployee?.position]);

  useEffect(() => {
    if (position?.headPositionId) {
      dispatch(getEmployeesByPositionId(position?.headPositionId));
      form?.resetFields(['headId']);
    }
  }, [position]);

  return (
    <Drawer
      title="Добавление нового сотрудника"
      placement="right"
      onClose={closeDrawerHandler}
      open={isDrawer}
    >
      <Space direction="vertical">
        <Form
          form={form}
          name="basic"
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: false }}
          onFinish={sendNewEmployeeHandler}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          onFieldsChange={handleFormChange}
        >
          <Form.Item
            label="Фамилия"
            name="lastName"
            rules={[
              { required: true, message: 'Пожалуйста, введите фамилию' },
              {
                pattern: new RegExp(/^[a-zа-яё\s]+$/iu),
                message: 'Фамилия должна включать только буквы',
              },
              {
                pattern: new RegExp(/^\S/),
                message: 'Поле не должно содержать лишних пробелов',
              },
            ]}
          >
            <Input value={newEmployee?.lastName} name="lastName" onChange={inputChangeHandler} />
          </Form.Item>
          <Form.Item
            label="Имя"
            name="firstName"
            rules={[
              { required: true, message: 'Пожалуйста, введите имя' },
              {
                pattern: new RegExp(/^[a-zа-яё\s]+$/iu),
                message: 'Имя должно включать только буквы',
              },
              {
                pattern: new RegExp(/^\S/),
                message: 'Поле не должно содержать лишних пробелов',
              },
            ]}
          >
            <Input name="firstName" value={newEmployee?.firstName} onChange={inputChangeHandler} />
          </Form.Item>
          <Form.Item
            label="Должность"
            name="position"
            rules={[{ required: true, message: 'Пожалуйста, выберите должность' }]}
          >
            <Select
              style={{ display: 'block', borderRadius: '9px' }}
              onChange={selectPositionHandler}
            >
              {user?.positionId?._id === process.env.REACT_APP_LEAD_POSITION
                ? positions
                    ?.filter((item) => item.headPositionId)
                    .map((position) => (
                      <Option key={position._id} value={position._id}>
                        {position.title}
                      </Option>
                    ))
                : positions.map((position) => (
                    <Option key={position._id} value={position._id}>
                      {position.title}
                    </Option>
                  ))}
            </Select>
          </Form.Item>
          {position?.headPositionId ? (
            <Form.Item
              label="Начальник"
              name="headId"
              rules={[{ required: true, message: 'Пожалуйста, выберите начальника' }]}
            >
              <Select
                style={{ display: 'block', borderRadius: '9px' }}
                onChange={selectHeadIdHandler}
              >
                {employeesFiltered.map((employee) => (
                  <Option key={employee._id} value={employee._id}>
                    {employee.lastName} {employee.firstName}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          ) : (
            ''
          )}
          <Form.Item
            label="Оклад"
            name="fixedSalary"
            rules={[
              { required: true, message: 'Пожалуйста, введите сумму оклада' },
              {
                pattern: new RegExp(/^[0-9]{1,}$/),
                message: 'Оклад должен состоять только из цифр',
              },
              {
                pattern: new RegExp(/^\S/),
                message: 'Поле не должно содержать лишних пробелов',
              },
            ]}
          >
            <Input
              name="fixedSalary"
              value={newEmployee?.fixedSalary}
              onChange={inputChangeHandler}
            />
          </Form.Item>
          <Form.Item
            label="Дата принятия"
            name="dateOfEmployment"
            rules={[{ required: true, message: 'Пожалуйста, выберите дату' }]}
          >
            <DatePicker onChange={onDateChange} />
          </Form.Item>
          <Form.Item
            label="Статус"
            name="status"
            rules={[{ required: true, message: 'Пожалуйста, выберите статус' }]}
          >
            <Select
              style={{ display: 'block', borderRadius: '9px' }}
              onChange={selectStatusHandler}
            >
              <Option value="Работает">Работает</Option>
              <Option value="Не работает">Не работает</Option>
            </Select>
          </Form.Item>
          <Form.Item wrapperCol={{ offset: 0 }}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ margin: '15px' }}
              disabled={disabledSave}
              value="large"
            >
              Сохранить
            </Button>
            <Button
              style={{ margin: '15px' }}
              onClick={closeDrawerHandler}
              value="large"
              type="primary"
              danger
            >
              Отмена
            </Button>
          </Form.Item>
        </Form>
      </Space>
    </Drawer>
  );
};
