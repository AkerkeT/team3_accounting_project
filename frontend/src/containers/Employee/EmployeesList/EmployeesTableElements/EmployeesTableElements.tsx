import React from 'react';
import { Button, Input, Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { DeleteOutlined, SearchOutlined } from '@ant-design/icons';
import { useAppSelector } from '../../../../hooks';
import { IEmployee } from '../../../../store/services/EmployeesListSlice';
import { IPosition } from '../../../../store/services/PositionSlice';

import 'antd/dist/antd.min.css';

interface TableProps {
  allEmployees: IEmployee[];
  showDrawer: (record: IEmployee) => void;
  deleteHandler: (record: IEmployee) => void;
  spinner: boolean;
}

export const EmployeesTableElements: React.FC<TableProps> = ({
  allEmployees,
  showDrawer,
  deleteHandler,
  spinner,
}) => {
  const renderPositionCell = (object: IPosition) => {
    return object.title;
  };
  const user = useAppSelector((state) => state.users.user?.user);

  const columns: ColumnsType<IEmployee> = [
    {
      key: 'firstName',
      title: 'Имя',
      dataIndex: 'firstName',
      sorter: (firtRec, scndRec) => {
        return firtRec.firstName > scndRec.firstName ? 1 : -1;
      },
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => {
        return (
          <Input
            placeholder="Введите текст..."
            value={selectedKeys[0]}
            onChange={(e) => {
              setSelectedKeys(e.target.value ? [e.target.value] : []);
              confirm({ closeDropdown: false });
            }}
            onPressEnter={(e) => {
              confirm({ closeDropdown: true });
              e.stopPropagation();
            }}
          />
        );
      },
      filterIcon: () => {
        return <SearchOutlined style={{ paddingLeft: 7, paddingRight: 7 }} />;
      },
      onFilter: (value: string | number | boolean, record: IEmployee) => {
        return record.firstName.toLowerCase().includes(String(value).toLowerCase());
      },
    },
    {
      key: 'lastName',
      title: 'Фамилия',
      dataIndex: 'lastName',
      sorter: (firtRec, scndRec) => {
        return firtRec.lastName > scndRec.lastName ? 1 : -1;
      },
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm }) => {
        return (
          <Input
            placeholder="Введите текст..."
            value={selectedKeys[0]}
            onChange={(e) => {
              setSelectedKeys(e.target.value ? [e.target.value] : []);
              confirm({ closeDropdown: false });
            }}
            onPressEnter={(e) => {
              confirm({ closeDropdown: true });
              e.stopPropagation();
            }}
          />
        );
      },
      filterIcon: () => {
        return <SearchOutlined style={{ paddingLeft: 7, paddingRight: 7 }} />;
      },
      onFilter: (value: string | number | boolean, record: IEmployee) => {
        return record.lastName.toLowerCase().includes(String(value).toLowerCase());
      },
    },
    {
      key: 'position',
      title: 'Должность',
      dataIndex: 'position',
      filters: [
        { text: 'Ведущий бухгалтер', value: 'Ведущий бухгалтер' },
        { text: 'Бухгалтер', value: 'Бухгалтер' },
        { text: 'Помощник бухгалтера', value: 'Помощник бухгалтера' },
      ],
      onFilter: (value, record: IEmployee) => {
        return record.position.title === value;
      },
      render: (object) => renderPositionCell(object),
    },
    {
      key: 'status',
      title: 'Статус',
      dataIndex: 'status',
    },
    {
      key: 'dateOfEmployment',
      title: 'Дата принятия',
      dataIndex: 'dateOfEmployment',
      render: (date: string) => new Date(date).toLocaleDateString(),
      sorter: (firtRec, scndRec) => {
        return firtRec.dateOfEmployment > scndRec.dateOfEmployment ? 1 : -1;
      },
    },
    {
      key: 'dateOfDismissal',
      title: 'Дата увольнения',
      dataIndex: 'dateOfDismissal',
      render: (date: string) => (date ? new Date(date).toLocaleDateString() : <p>-</p>),
      sorter: (firtRec, scndRec) => {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        return firtRec.dateOfDismissal! > scndRec.dateOfDismissal! ? 1 : -1;
      },
    },
    {
      key: 'more',
      title: 'Действие',
      render: (record: IEmployee) => {
        return (
          <Button type="primary" onClick={() => showDrawer(record)}>
            {' '}
            Подробнее
          </Button>
        );
      },
    },
  ];

  const deleteColumn = {
    key: 'delete',
    title: 'Удаление',
    render: (record: IEmployee) => {
      return record.status !== 'Удалён' ? (
        <Button className="ant-custom-btn" onClick={() => deleteHandler(record)}>
          <DeleteOutlined style={{ color: 'red', fontSize: '20px' }} />
        </Button>
      ) : null;
    },
  };

  if (user?.roleId === process.env.REACT_APP_MAIN_ROLE) columns.push(deleteColumn);

  return (
    <Table
      locale={{
        triggerDesc: 'Нажмите для сортировки по убыванию',
        triggerAsc: 'Нажмите для сортировки по возрастанию',
        cancelSort: 'Отменить сортировку текста',
      }}
      dataSource={allEmployees}
      columns={columns}
      rowKey={(obj) => obj._id}
      loading={spinner}
    />
  );
};
