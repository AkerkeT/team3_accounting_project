import { Request, Response } from 'express';
import { RoleModel } from '../models/Role';

export const checkRole = (roles: string[]) => {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return async (req: Request, res: Response, next: Function) => {
    const role = await RoleModel.findById(req.body.user.role);

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    if (!roles.includes(role!.title)) {
      return res.status(403).send({ message: 'У Вас нет доступа' });
    }
    return next();
  };
};

export default checkRole;
