import dotenv from 'dotenv';
import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';

// eslint-disable-next-line @typescript-eslint/ban-types
function auth(req: Request, res: Response, next: Function) {
  try {
    dotenv.config();
    const { SECRET } = process.env;
    const token = req.get('Authorization');
    if (!token) {
      return res.status(401).send({ message: 'User is not authorized' });
    }
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const decodedData = jwt.verify(token, SECRET!);
    req.body.user = decodedData;
    return next();
  } catch (error) {
    return res.status(400).send(error);
  }
}

export default auth;
