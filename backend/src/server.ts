import cors from 'cors';
import dotenv from 'dotenv';
import express, { Express, Request, Response } from 'express';
import mongoose from 'mongoose';
import clientHocService from './routes/admin/clientHocService';
import clients from './routes/admin/clients';
import employees from './routes/admin/employees';
import factForMonth from './routes/admin/factForMonth';
import kpi from './routes/admin/kpi';
import kpiHistory from './routes/admin/kpiHistory';
import kpiKeyIndicator from './routes/admin/kpiKeyIndicators';
import mainPage from './routes/admin/mainPage';
import positions from './routes/admin/positions';
import rates from './routes/admin/rates';
import ratesEmployee from './routes/admin/ratesEmployee';
import roles from './routes/admin/roles';
import serviceElements from './routes/admin/serviceElements';
import servicePayment from './routes/admin/servicePayment';
import servicePaymentHistory from './routes/admin/ServicePaymentHistory';
import employeeClients from './routes/employees/employeeClients';
import employeeKpi from './routes/employees/employeeKpi';
import employeesSubordinates from './routes/employees/employeesSubordinates';
import kpiFacts from './routes/employees/kpiFacts';
import users from './routes/users';

dotenv.config();

const app: Express = express();
const port = process.env.PORT;
const mongoUrl = process.env.MONGO_URL;
const databaseName = process.env.MONGO_DATABASE_NAME;
app.use(cors());
app.use(express.json());
app.use('/users', users);
app.use('/roles', roles);
app.use('/admin/clients', clients);
app.use('/admin/employees', employees);
app.use('/admin/positions', positions);
app.use('/admin/serviceElements', serviceElements);
app.use('/admin/rates', rates);
app.use('/admin/ratesEmployee', ratesEmployee);
app.use('/admin/kpi', kpi);
app.use('/admin/client/servicePayment', servicePayment);
app.use('/admin/client/servicePaymentHistory', servicePaymentHistory);
app.use('/admin/client/hocService', clientHocService);
app.use('/admin/kpiKeyIndicator', kpiKeyIndicator);
app.use('/admin/KpiHistory', kpiHistory);
app.use('/employee/clients', employeeClients);
app.use('/employee/subordinates', employeesSubordinates);
app.use('/admin/factForMonth', factForMonth);
app.use('/employee/kpi', employeeKpi);
app.use('/employee/kpiFact', kpiFacts);
app.use('/main', mainPage);

app.get('/', (req: Request, res: Response) => {
  res.send('Express + TypeScript Server');
});

const run = async () => {
  try {
    if (mongoUrl) {
      mongoose.connect(`${mongoUrl}/${databaseName}`);
    } else {
      throw new Error('Can not connect database');
    }

    app.listen(port, () => {
      console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
    });

    process.on('exit', () => {
      mongoose.disconnect();
    });
  } catch (error) {
    console.log(error);
  }
};

run().catch(console.error);
