import express, { Request, Response } from 'express';
import { Employee } from 'models/Employee/Employee';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { Client } from '../../models/Client/Client';
import { ClientAddHocServiceModel } from '../../models/ClientAddHocService';

const router = express.Router();

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { serviceElements, startDate, endDate, payDate, sum, description, employeeId, clientId } =
    req.body;
  try {
    const service = await ClientAddHocServiceModel.create({
      serviceElements: serviceElements,
      startDate: startDate,
      endDate: endDate,
      payDate: payDate,
      sum: sum,
      description: description || null,
      employeeId: employeeId,
      clientId: clientId,
    });
    const newService = await ClientAddHocServiceModel.find({ _id: service._id })
      .populate<{
        employeeId: Employee;
      }>('employeeId')
      .exec();
    return res.status(200).send(newService[0]);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.get('/', async (req: Request, res: Response) => {
  if (req.query.client) {
    const service = await ClientAddHocServiceModel.find({ clientId: req.query.client })
      .populate<{
        clientId: Client;
      }>('clientId')
      .populate<{
        employeeId: Employee;
      }>('employeeId')
      .exec();
    return res.status(200).send(service);
  }
  try {
    const service = await ClientAddHocServiceModel.find().exec();
    return res.status(200).send(service);
  } catch (error) {
    return res.status(400).send(error);
  }
});

export default router;
