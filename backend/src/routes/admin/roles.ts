import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { RoleModel } from '../../models/Role';

const router = express.Router();

router.get('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const userRoles = await RoleModel.find({
      $where: function () {
        return this.title !== 'admin';
      },
    });
    return res.status(200).send(userRoles);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    await RoleModel.create({ title: req.body.title });
    return res.send(`Роль ${req.body.title} был успешно создан`);
  } catch (e) {
    return res.status(400).send(e);
  }
});

export default router;
