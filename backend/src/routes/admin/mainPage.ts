/* eslint-disable prettier/prettier */
import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import { checkRole } from '../../middleware/checkRole';
import { PositionModel } from '../../models/catalogs/Positions/Position';
import { ClientModel, ClientStatus } from '../../models/Client/Client';
import { Employee, EmployeeModel, EmployeeStatus } from '../../models/Employee/Employee';
import { ServicePaymentModel } from '../../models/ServicePayments/ServicePayment';
const router = express.Router();

router.get('/',[auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  if(req.query.payment){
    const payment = await ServicePaymentModel.find();
    const activeClients = await ClientModel.find({_id: payment.map(pay => pay.clientId), status: ClientStatus.active});
    const paymentActive = await ServicePaymentModel.find({clientId: activeClients.map((active: { _id: string }) => active._id)});
    const totalSumPayment = paymentActive.map((total: { totalSum: number }) => {
      return total.totalSum
    });
    let totalPayment = 0;
    totalSumPayment.map(total => {
      return totalPayment += total, totalPayment
    }).reverse()[0];
    return res.send({totalPayment});
  }
  const employeesArr: Employee[] = [];
  const title = 'Ведущий бухгалтер';
  const positionId = await PositionModel.findOne({ title: title });
  const employees = await EmployeeModel.find({
    position: positionId?._id,
    status: EmployeeStatus.works,
  });
  let indx = 0;
  for (const item of employees) {
    const obj = {
      ...item,
      emp: await EmployeeModel.find({
        headId: item._id,
        status: EmployeeStatus.works,
      }),
      index: indx += 1,
    };
    employeesArr.push(obj);
  }

  return res.send(employeesArr);
});

export default router;
