import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { RatesEmployee } from '../../models/catalogs/Rates/RatesEmployee';

const router = express.Router();

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  const { position, percent } = req.body;
  if (!position) return res.status(400).send({ message: 'Должность не указан.' });
  if (!percent) return res.status(400).send({ message: 'Процент не указан.' });
  try {
    const positionEmployee = await RatesEmployee.findOne({ position });
    if (positionEmployee?.position) {
      return res.send({
        message: `Тариф для должности "${positionEmployee?.position}" уже добавлен!`,
      });
    } else {
      const rate = await RatesEmployee.create({ position, percent });
      return res
        .status(200)
        .send({ message: `Тариф для должности "${position}" добавлен успешно!`, rate });
    }
  } catch (error) {
    return res.status(400).send({ message: 'Тариф для должности не добавлен', error });
  }
});

router.get('/', [auth], async (req: Request, res: Response) => {
  try {
    const rate = await RatesEmployee.find().sort({ position: 1 }).exec();
    return res.status(200).send(rate);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.put('/:id', async (req: Request, res: Response) => {
  const { position, percent } = req.body;
  if (!position) return res.status(400).send({ text: 'Должность не указан.' });
  if (!percent) return res.status(400).send({ text: 'Процент не указан.' });
  try {
    const rate = await RatesEmployee.findByIdAndUpdate(req.params.id, {
      position,
      percent,
    }).exec();
    return rate
      ? res
          .status(200)
          .send({ message: `Тариф для должности "${position}" изменен успешно!`, rate })
      : null;
  } catch (e) {
    return res.status(400).send({ message: 'Ошибка, тариф для должности не изменен', e });
  }
});
export default router;
