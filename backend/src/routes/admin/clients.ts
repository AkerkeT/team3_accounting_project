import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import checkRole from '../../middleware/checkRole';
import { Client, ClientModel, ClientStatus } from '../../models/Client/Client';
import { createClient } from '../../models/Client/methods/createClient';
import { Employee, EmployeeModel } from '../../models/Employee/Employee';
const router = express.Router();

router.post('/', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const newClient = await createClient(req.body);

    return res.send({ message: 'Клиент создана успешно', newClient });
  } catch (e: any) {
    return res.status(400).send({ message: e?.message });
  }
});

router.get('/', [auth, checkRole(['admin', 'user'])], async (req: Request, res: Response) => {
  if (req?.query?.status) {
    try {
      const status = (req.query.status as string).split(',');
      const clients = await ClientModel.find({
        status: { $in: status },
      })
        .sort({ registrationDate: -1 })
        .exec();
      return res.send(clients);
    } catch (e) {
      return res.status(400).send(e);
    }
  }
  if (req.query.accompanyingEmployees) {
    try {
      const filterClients: Client[] = [];
      const clients = await ClientModel.find().exec();
      clients.map((client) => {
        if (client.accompanyingEmployees?.length === 0 && client.status !== 'Удалён') {
          filterClients.push(client);
        } else {
          return;
        }
      });
      return res.send(filterClients);
    } catch (e) {
      return res.status(400).send(e);
    }
  }
  try {
    const clients = await ClientModel.find({
      status: [ClientStatus.active, ClientStatus.notActive],
    })
      .sort({ registrationDate: -1 })
      .exec();

    return res.send(clients);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.get('/:id', [auth, checkRole(['admin', 'user'])], async (req: Request, res: Response) => {
  try {
    const client = await ClientModel.findById(req.params.id)
      .populate<{
        accompanyingEmployees: Employee;
      }>('accompanyingEmployees')
      .populate({
        path: 'accompanyingEmployees',
        populate: {
          path: 'position',
        },
      })
      .exec();

    return res.send(client);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.patch('/:id', [auth, checkRole(['admin', 'user'])], async (req: Request, res: Response) => {
  if (req.query.accompanyingEmployees) {
    try {
      const clientId = req.params.id;
      const addAccomEmployee = req.body.accompanyingEmployee;
      if (!clientId)
        return res.status(400).send({ message: 'Не переданы параметры поиска Клиента' });

      if (!addAccomEmployee)
        return res.status(400).send({ message: 'Не переданы данные сопровождаемого сотрудника' });

      await ClientModel.findOneAndUpdate(
        { _id: clientId },
        {
          $push: {
            accompanyingEmployees: addAccomEmployee,
          },
        }
      );
      const addedEmployee = await EmployeeModel.find({ _id: addAccomEmployee }).exec();
      return res.send(addedEmployee);
    } catch (e) {
      return res.status(400).send(e);
    }
  } else {
    try {
      const clients = await ClientModel.findByIdAndUpdate(req.params.id, {
        status: req.body.status,
      }).exec();
      return res.send(clients);
    } catch (e) {
      return res.status(400).send(e);
    }
  }
});

router.put('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  if (!req.body.typeOfEconomicActivity) {
    return res.status(400).send({ message: 'Вид деятельности должна быть заполнено.' });
  }
  if (!req.body.status) {
    return res.status(400).send({ message: 'Статус должна быть заполнено.' });
  }
  if (!req.body.name) {
    return res.status(400).send({ message: 'Наименование клиента должна быть заполнено.' });
  }
  try {
    const client = await ClientModel.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    })
      .populate<{
        accompanyingEmployees: Employee;
      }>('accompanyingEmployees')
      .populate({
        path: 'accompanyingEmployees',
        populate: {
          path: 'position',
        },
      })
      .exec();
    return res.send({ message: 'Данные успешно изменены', client });
  } catch (e) {
    return res.status(400).send(e);
  }
});

export default router;
