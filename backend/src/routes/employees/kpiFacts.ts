import express, { Request, Response } from 'express';
import auth from '../../middleware/auth';
import { checkRole } from '../../middleware/checkRole';
import { KPIFactModel } from '../../models/KPIFact/KPIFact';
import { UserModel } from '../../models/User/User';

const router = express.Router();

router.post('/', [auth, checkRole(['user'])], async (req: Request, res: Response) => {
  const { employeeId, year, month, kpiFactElements, totalSalary, hocServices } = req.body;

  if (!year) return res.status(400).send({ message: 'Год не заполнен' });
  if (!month) return res.status(400).send({ message: 'Месяц не заполнен' });

  try {
    const isExist = await KPIFactModel.find({
      employeeId: employeeId,
      year: year,
      month: month,
    }).exec();

    if (isExist.length === 0) {
      const newKpiFact = KPIFactModel.create({
        employeeId: employeeId,
        year: year,
        month: month,
        status: 'Создан',
        kpiFactElements: kpiFactElements,
        createdDate: new Date().toLocaleString(),
        hocPayments: hocServices,
        totalSalary: totalSalary,
      });

      return res.status(200).send({ message: 'факт KPI добавлен успешно', newKpiFact });
    } else {
      return res.status(400).send({
        message: `Факт KPI за ${req.body.month} ${req.body.year} года уже был создан ранее`,
      });
    }
  } catch (error) {
    return res.status(400).send({ message: 'факт KPI не добавлен', error });
  }
});

router.get(
  '/employeesKpiFacts',
  [auth, checkRole(['user'])],
  async (req: Request, res: Response) => {
    const userInfo = req.body.user;
    const employeeId = await UserModel.findById(userInfo.id).then(async (user) => {
      if (!user) return;
      return user?.employeeId;
    });
    if (!employeeId) return res.status(400).send({ message: 'Пользователь не найден' });

    try {
      const kpiFact = await KPIFactModel.find({ employeeId: employeeId }).exec();
      return res.send(kpiFact);
    } catch (error) {
      return res.status(400).send(error);
    }
  }
);

router.get('/allYears', [auth, checkRole(['user'])], async (req: Request, res: Response) => {
  const userInfo = req.body.user;
  const employeeId = await UserModel.findById(userInfo.id).then(async (user) => {
    if (!user) return;
    return user?.employeeId;
  });
  if (!employeeId) return res.status(400).send({ message: 'Пользователь не найден' });

  try {
    const kpiFact = await KPIFactModel.find({ employeeId: employeeId }).sort({ year: 1 }).exec();
    const onlyYears = kpiFact.map((element) => element.year);
    return res.send(onlyYears);
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.get('/:year', [auth, checkRole(['user'])], async (req: Request, res: Response) => {
  const userInfo = req.body.user;
  const employeeId = await UserModel.findById(userInfo.id).then(async (user) => {
    if (!user) return;
    return user?.employeeId;
  });
  try {
    const elements = await KPIFactModel.find({ employeeId: employeeId })
      .find({ year: req.params.year })
      .exec();
    if (elements.length === 0) {
      return res
        .status(404)
        .send({ message: `Нет созданных KPI за ${req.params.year} год`, elements });
    }
    return res.status(200).send(elements);
  } catch (error) {
    return res
      .status(404)
      .send({ message: `Нет созданных фактов за KPI ${req.params.year} год`, error });
  }
});

export default router;
