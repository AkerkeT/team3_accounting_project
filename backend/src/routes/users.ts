import bcrypt from 'bcrypt';
import dotenv from 'dotenv';
import express, { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import auth from '../middleware/auth';
import { checkRole } from '../middleware/checkRole';
import { Position } from '../models/catalogs/Positions/Position';
import { Employee } from '../models/Employee/Employee';
import { RoleModel } from '../models/Role';
import { generateToken } from '../models/User/methods/generateToken';
import { UserModel } from '../models/User/User';

const router = express.Router();
dotenv.config();
interface JwtPayload {
  iat: string | undefined;
  exp: string | undefined;
}

router.get('/:id', async (req: Request, res: Response) => {
  try {
    const user = await UserModel.findOne({ employeeId: req.params.id });
    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post('/register', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    const { login, password, positionId, employeeId } = req.body;
    const checkLogin = await UserModel.findOne({ login });
    if (checkLogin) {
      return res.status(400).send({ message: `Пользователь с логином ${login} уже существует` });
    }
    const role = await RoleModel.findOne({ title: 'user' });
    if (!role) {
      return res.status(400).send({ message: `Роль пользователя не найдена` });
    }
    if (password.length < 8)
      return res.status(400).send({ message: 'Длина пароля должна быть больше 8 значений' });

    const userData = { login, password, roleId: role._id, positionId, employeeId };
    const user = await UserModel.create(userData);
    return res.status(200).send({ message: 'Пользователь успешно зарегистрирован', user });
  } catch (error) {
    return res.status(400).send({ message: 'Ошибка пользователь не зарегестрирован', error });
  }
});

router.post('/login', async (req: Request, res: Response) => {
  dotenv.config();
  const { SECRET } = process.env;
  try {
    const { login, password } = req.body;
    const user = await UserModel.findOne(
      { login },
      {
        login: 1,
        roleId: 1,
        employeeId: 1,
        positionId: 1,
        password: 1,
      }
    )
      .populate<{ employeeId: Employee }>('employeeId')
      .populate<{ positionId: Position }>('positionId')
      .exec();

    if (!user) {
      return res.status(400).send({ message: `Неверный логин или пароль` });
    }

    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) return res.status(400).send({ message: 'Неверный логин или пароль' });
    const token = await generateToken({ id: user._id, role: user.roleId });

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const { iat, exp } = jwt.verify(token, SECRET!) as JwtPayload;

    const userInfo = { tokenData: { token, iat, exp }, user };

    return res.status(200).send({ userInfo });
  } catch (error) {
    return res.status(400).send(error);
  }
});

router.delete('/:id', [auth, checkRole(['admin'])], async (req: Request, res: Response) => {
  try {
    if (!req.params.id) {
      return res.status(400).send({ message: 'Не указаны параметры поиска' });
    }

    const id = req.params.id;
    const deletedUser = await UserModel.deleteOne({ _id: id });

    if (!deletedUser) {
      return res.status(400).send({ message: 'Пользователь не найден' });
    }

    return res.status(200).send({ message: 'Пользователь успешно удален' });
  } catch (error) {
    return res.status(400).send({ message: 'Что-то пошло не так!', error });
  }
});

router.patch('/password/:id', async (req: Request, res: Response) => {
  try {
    if (!req.params.id) {
      return res.status(400).send({ message: 'Не указаны параметры поиска' });
    }
    const hashPassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10));
    if (req.body.password.length < 8)
      return res.status(400).send({ message: 'Длина пароля должна быть больше 8 значений' });
    const changeUserPass = await UserModel.findByIdAndUpdate(req.params.id, {
      password: hashPassword,
    });

    if (!changeUserPass) {
      return res.status(400).send({ message: 'Пользователь не найден' });
    }
    return res.send({ message: 'Пароль успешно изменен', changeUserPass });
  } catch (error) {
    return res.status(400).send({ message: 'Что-то пошло не так!', error });
  }
});

export default router;
