import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';
import { Client } from '../Client/Client';

export enum FactStatus {
  created = 'Создан',
  finished = 'Обработан',
}
export class FactServiceElement {
  @prop({ required: true, type: String })
  public title: string;

  @prop({ required: true, type: Number })
  public quantity: number;

  @prop({ required: true, type: Number })
  public price: number;

  @prop({ required: true, type: Number })
  public factQuantity: number;

  @prop({ required: true, type: Number })
  public sum: number;

  @prop({ required: true, type: Number })
  public priceForEmployee: number;
}
export class ActiveClientsFactOfService {
  @prop({ required: true, type: String })
  _id: string;

  @prop({ required: true, type: Date })
  startDate: Date;

  @prop({ required: true, type: Number })
  totalSum: number;

  @prop({ required: true, type: Client })
  clientId: Client;

  @prop({ required: true, type: Number })
  factTotalSum: number;

  @prop({ required: true, type: Number })
  difference: number;

  @prop({ required: true, type: FactServiceElement })
  serviceElements: FactServiceElement[];

  @prop({ required: true, type: String })
  status: string;
}

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'FactForMonth' },
  schemaOptions: {
    collection: 'factForMonth',
  },
})
export class FactForMonth {
  @prop({ required: true, type: Number })
  public year!: number;

  @prop({ required: true, type: Number })
  public month!: number;

  @prop({ type: ActiveClientsFactOfService })
  public activeClientsFactOfService!: Array<ActiveClientsFactOfService>;

  @prop({ required: true, enum: FactStatus, type: String })
  public status!: FactStatus;

  @prop({ required: true, type: Number, default: 0 })
  public monthfactTotalSum!: number;

  @prop({ required: true, type: Number, default: 0 })
  public monthTotalServicePaymentsFee: number;

  @prop({ required: true, type: Number, default: 0 })
  public difference: number;
}

export const FactForMonthModel = getModelForClass(FactForMonth);
