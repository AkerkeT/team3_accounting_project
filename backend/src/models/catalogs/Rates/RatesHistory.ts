import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';
import mongoose from 'mongoose';

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'RatesHistory' },
  schemaOptions: {
    collection: 'RatesHistory',
  },
})
export class RatesHistory {
  @prop({ type: mongoose.Types.ObjectId, ref: 'Rates' })
  public rateId?: string;

  @prop({ required: true, type: String })
  public elementsOfService!: string;

  @prop({ required: true, type: String })
  public price?: string;

  @prop({ type: Date })
  public dateOfEvent?: Date;
}

export const RatesHistoryModel = getModelForClass(RatesHistory);
