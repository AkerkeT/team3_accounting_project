import { getModelForClass, modelOptions, prop, Ref, Severity } from '@typegoose/typegoose';

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'Position' },
  schemaOptions: {
    collection: 'position',
  },
})
export class Position {
  @prop({ required: true, type: String, unique: true })
  public title!: string;

  @prop({ ref: () => Position })
  public headPositionId!: Ref<Position> | null;
}

export const PositionModel = getModelForClass(Position);
