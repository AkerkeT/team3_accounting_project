import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';
import mongoose from 'mongoose';
import { KpiElements } from './KpiToPosition';

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'kpiCatalogHistory' },
  schemaOptions: {
    collection: 'kpiCatalogHistory',
  },
})
export class KpiHistory {
  @prop({ type: mongoose.Types.ObjectId, ref: 'kpiCatalog' })
  public kpiId?: string;

  @prop({ required: true })
  public kpiElements!: Array<KpiElements>;

  @prop({ type: Number, default: 0 })
  public percentForKpi?: number;

  @prop({ type: mongoose.Types.ObjectId, ref: 'Position' })
  public positionId?: string;

  @prop({ type: Date })
  public dateOfEvent?: Date;
}

export const KpiCatalogHistoryModel = getModelForClass(KpiHistory);
