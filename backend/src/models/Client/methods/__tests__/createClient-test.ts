import { beforeAll, describe, expect, jest, test } from '@jest/globals';
import { ClientModel } from '../../Client';
import { createClient } from '../createClient';
jest.setTimeout(180000);
const client = {
  bin: '123456798123',
  typeOfEconomicActivity: 'Розничная торговля компьютерами, периферийным оборудованием',
  status: 'Активный',
  registrationDate: new Date('2023-01-25T05:52:25.138+00:00'),
  expirationDate: null,
  name: 'ТОО БЕЛЫЙ ВЕТЕР KZ',
  accompanyingEmployees: [],
};

describe('models.Client.methods.createClient', () => {
  beforeAll(async () => {
    ClientModel.create = jest.fn<any>().mockResolvedValue(client);
  });

  test('позитивная проверка на создание Клиента', async () => {
    const newClient = await createClient(client);
    expect(newClient).toMatchObject(client);
  });

  describe('Негативная проверка на создание Клиента', () => {
    test('Некорректный IIN/BIN, менее 12 символов', async () => {
      client.bin = '6544598768';

      await expect(createClient(client)).rejects.toThrowError(
        new Error('IIN or BIN is not correct')
      );
    });

    test('Не передано поле IIN/BIN,', async () => {
      client.name = 'ТОО БЕЛЫЙ ВЕТЕР KZ';
      client.bin = '';
      await expect(createClient(client)).rejects.toThrowError(new Error('No data on IIN and BIN'));
    });

    test('Не передано поле - ВЭД,', async () => {
      client.bin = '123456798123';
      client.typeOfEconomicActivity = '';

      await expect(createClient(client)).rejects.toThrowError(
        new Error('Вид деятельности должна быть заполнено.')
      );
    });

    test('Не передано поле - Статус,', async () => {
      client.typeOfEconomicActivity = 'Розничная торговля компьютерами, периферийным оборудованием';
      client.status = '';

      await expect(createClient(client)).rejects.toThrowError(
        new Error('Статус должна быть заполнено.')
      );
    });

    test('Не передано поле - Наименование,', async () => {
      client.status = 'Активный';
      client.name = '';
      await expect(createClient(client)).rejects.toThrowError(
        new Error('Наименование клиента должна быть заполнено.')
      );
    });
  });
});
