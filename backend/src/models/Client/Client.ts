import { getModelForClass, modelOptions, prop, Severity } from '@typegoose/typegoose';
import mongoose from 'mongoose';
import { Employee } from '../Employee/Employee';

export enum ClientStatus {
  active = 'Активный',
  notActive = 'Неактивный',
  deleted = 'Удалён',
}
@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'Client' },
  schemaOptions: {
    collection: 'client',
  },
})
export class Client {
  @prop({
    type: String,
    validate: {
      validator: (value: string) => {
        return /^[0-9]{1,}$/.test(value);
      },
      message: 'IIN is not correct',
    },
  })
  public iin?: string;

  @prop({
    type: String,
    validate: {
      validator: (value: string) => {
        return /^[0-9]{1,}$/.test(value);
      },
      message: 'BIN is not correct',
    },
  })
  public bin?: string;

  @prop({ required: true, type: String })
  public name!: string;

  @prop({ required: true, type: String })
  public typeOfEconomicActivity!: string;

  @prop({ required: true, enum: ClientStatus, type: String })
  public status!: ClientStatus;

  @prop({ required: true, type: Date })
  public registrationDate!: Date;

  @prop({ type: Date || null })
  public expirationDate!: Date | null;

  @prop({ type: mongoose.Types.ObjectId, ref: 'Employee' })
  public accompanyingEmployees?: Employee[];
}

export const ClientModel = getModelForClass(Client);
